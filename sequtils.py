# Compares two sequence possibly having "N" character             
def compare_seqs(this_seq, that_seq):
    flagged = True
    if len(this_seq) != len(that_seq) :
        return False
    elif "N" not in this_seq and "N" not in that_seq:
        if this_seq == that_seq :
            return True
        else:
            return False
    for each_position in range(len(this_seq)):
        if this_seq[each_position] == that_seq[each_position] or this_seq[each_position] == "N" or \
            that_seq[each_position] == "N":
            flagged = flagged and True
            
        else:
            flagged = False
            return False
    return True
    
# If two sequence are counted equal
# generatea consensus
def choose_less_amb_one(parent_one, parent_two):
    new_seq = ""
    for this_pos in range(max(len(parent_two),len(parent_one))):
        one_pos = parent_one[this_pos]
        two_pos = parent_two[this_pos]
        if one_pos != "N" :
            new_seq += one_pos
        elif two_pos != "N" :
            new_seq += two_pos
        else:
            new_seq+="N"
    
    #To see if we have a better version 
    '''
    changed_in = []
    if new_seq != parent_one:
        changed_in.append("a")
    if new_seq != parent_two:
        changed_in.append("b")
    '''
    return new_seq
    
# Compares two sequence possibly having "N" character             
def compare_seqs_flexible(this_seq, that_seq):
    errcounter = 0
    if len(that_seq) > len(this_seq) :
        this_seq, that_seq =  that_seq, this_seq
    if len(this_seq) - len(that_seq) == 1:
        errcounter+=1
        for i in range(len(this_seq)):
            if this_seq[:i]+ this_seq[i+1:] == that_seq:
                this_seq = this_seq[:i]+ this_seq[i+1:]
                break
                
    
    if len(this_seq) == len(that_seq):
        for each_position in range(len(this_seq)):
            if not (this_seq[each_position] == that_seq[each_position] or this_seq[each_position] == "N" or \
                that_seq[each_position] == "N"):
                    errcounter+=1
            
        if errcounter <= 1:
            return True
    
    return False
            