from difflib import SequenceMatcher
import needle
from collections import defaultdict
import treeutils
import const
import random
import math
import evaluate
import forward
import probcalculator 

class GREEDYJOIN():
    def __init__(self):
        self.leaves = []
        self.prefix_table = defaultdict(list)
        self.children = defaultdict(list)
        self.prefixes = defaultdict(list)
        self.tree = [[]]* (2*(2**const.LEVEL)-1)
        self.t_size = 2*(2**const.LEVEL)-1
        self.post_dict = treeutils.gen_postx(self.t_size)
        self.tree1 = {}
        self.tree2 = {}


    def tree_eq(self,ind1,ind2):
        if len(ind1) > (const.LEVEL+1 ) or len(ind2) > (const.LEVEL+1):
            return True
        if self.tree1[ind1] != self.tree2[ind2]:
            return False
        if self.tree_eq(ind1+"0",ind2+"0") and self.tree_eq(ind1+"1", ind2+"1"):
            return True
        if self.tree_eq(ind1+"0",ind2+"1") and self.tree_eq(ind1+"1", ind2+"0"):
            return True
        return False    


    def calculate_tree_probs(self, tree):
        probs  = 0
        for ind in range(2**const.LEVEL - 1):
            #if probcalculator.calc_prbXY(tree[ind], tree[2*ind+1]) == float('-inf'):
            #    print ind
            #if probcalculator.calc_prbXY(tree[ind], tree[2*ind+1]) == float('-inf'):
            #    print "and  " +str(ind)
            probs+= probcalculator.calc_prbXY(tree[ind], tree[2*ind+1])
            probs+= probcalculator.calc_prbXY(tree[ind], tree[2*ind+2])
            
        return probs
        
        
    def find_possible_parent(self, seq1, seq2):
        s = SequenceMatcher()
        #if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
        #    s.set_seq1(seq1[:-4])
        #    s.set_seq2(seq2[:-4])
        #    matches = s.get_matching_blocks()
        if seq1!=seq2 and seq1[-3:] == seq2[-3:]:
            s.set_seq1(seq1[:-3])
            s.set_seq2(seq2[:-3])
            matches = s.get_matching_blocks()
        else:
            s.set_seq1(seq1)
            s.set_seq2(seq2)
            matches = s.get_matching_blocks()
            
        #if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
        #    parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:] 

        if seq1!=seq2 and seq1[-3:] == seq2[-3:]:
            parent = seq1[matches[0][0]:matches[0][2]] + seq1[-3:]
            
        elif len(matches) >= 3 and len(seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]) in [3,4]: 
            parent = seq1[matches[0][0]:matches[0][2]]+seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]
            #print seq1,seq2, parent
        elif seq1 == seq2 :
            parent = seq1
        elif seq1.endswith("TGA") or seq2.endswith("TGA"):
            parent = seq1[matches[0][0]:matches[0][2]] + "TGA"
        else:
            parent = seq1[matches[0][0]:matches[0][2]] 
        
        if abs(len(parent)-len(seq1)) <= max(const.INS_LEN_PRB.keys()) and  abs(len(parent)-len(seq2)) <= max(const.INS_LEN_PRB.keys()) :
            return parent
        else:
            if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
                s.set_seq1(seq1[:-4])
                s.set_seq2(seq2[:-4])
                matches = s.get_matching_blocks()
                parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:]
                if abs(len(parent)-len(seq1)) <= max(const.INS_LEN_PRB.keys()) and  abs(len(parent)-len(seq2)) <= max(const.INS_LEN_PRB.keys()) :
                    return parent
            elif seq1!=seq2 and seq1[-3:] == seq2[-3:]:                
                s.set_seq1(seq1[:-3])
                s.set_seq2(seq2[:-3])
                matches = s.get_matching_blocks()
                check_1 = seq1[-4]
                check_2 = seq2[matches[0][2]+1]
                
                check_3 = seq2[-4]
                check_4 = seq1[matches[0][2]+1]
                
                if check_1==check_2:
                    parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:]
                elif check_3 == check_4:
                    parent = seq1[matches[0][0]:matches[0][2]] + seq2[-4:]
                if abs(len(parent)-len(seq1)) <= max(const.INS_LEN_PRB.keys()) and  abs(len(parent)-len(seq2)) <= max(const.INS_LEN_PRB.keys()) :
                    return parent                
            return ""
            
    def find_common_blocks(self, seq1, seq2):
        blocks = needle.needle(seq1, seq2).split(" ")
        return blocks
        
        
    def pair_seqs(self, this_level_num):
        these_prefixes = self.prefixes[this_level_num]
        rng = range(len(these_prefixes))
        self.prefix_table[this_level_num] = [[[] for i in rng] for j in rng]
        for i in range(len(these_prefixes)):
            for j in range(i,len(these_prefixes)):
                if i == j :
                    self.prefix_table[this_level_num][i][j]= ""
                else:
                    self.prefix_table[this_level_num][i][j] = self.find_possible_parent(these_prefixes[i], these_prefixes[j])
                    self.prefix_table[this_level_num][j][i] = self.find_possible_parent(these_prefixes[i], these_prefixes[j])
                    
                    
    def greedy_pairs(self,this_level_num):
        max_len = -1
        pairs = []
        indexes = range(2**this_level_num)
        ending = (len(indexes)/2) 
        while len(pairs) < ending :
            max_len = -1
            for i in range(len(indexes)-1):
                for j in range(i+1, len(indexes)):                    
                    if len(self.prefix_table[this_level_num][indexes[i]][indexes[j]]) >= max_len:
                        max_len = len(self.prefix_table[this_level_num][indexes[i]][indexes[j]])                 
                        max_pair = [indexes[i],indexes[j]]
            to_be_removed1 = max_pair[0]
            to_be_removed2 = max_pair[1]
            indexes.remove(to_be_removed1)
            indexes.remove(to_be_removed2)
            pairs.append(max_pair)
        for p in pairs:
            self.prefixes[this_level_num-1].append(self.prefix_table[this_level_num][p[0]][p[1]])
            self.children[this_level_num-1].append((p[0],p[1]))
        


    def greedy_pairs_all(self):
        for this_level_num in range(const.LEVEL, 0, -1):
            self.pair_seqs(this_level_num)
            self.greedy_pairs(this_level_num)
            
        self.feel_tree()
        
        
    def fill_children(self, k, m):
        child_level = treeutils.get_layer_fromtreeid(k)
        if child_level < const.LEVEL:
            these_children = self.children[child_level][m]
            self.tree[2*k+1] = (self.prefixes[child_level+1][these_children[0]],these_children[0])
            self.tree[2*k+2] = (self.prefixes[child_level+1][these_children[1]],these_children[1])   
            self.fill_children(2*k+1, these_children[0])
            self.fill_children(2*k+2, these_children[1])           
        
        
    def feel_tree(self):        
        self.tree[0] = (self.prefixes[0][0],0)
        self.fill_children(0,0)
            
    
    def njprocess(self, leaves):
        self.prefixes[const.LEVEL] = leaves
        self.greedy_pairs_all()

    def rungreedyjoin(self, leaves, rounds):
        possible_leaves = []
        for perm in range(rounds):
            leaves_ids = range(len(leaves))
            l = list(zip(leaves, leaves_ids))
            random.shuffle(l)
            shuffled_leaves, shuffled_ids = zip(*l)

            iddict = {}
            for i in range(len(leaves)):
                iddict[i] = shuffled_ids[i]
            self.njprocess(shuffled_leaves)

            these_possible_tree = []
            for node in self.tree[:-len(leaves)]:
                these_possible_tree.append(node[0])
            for node in self.tree[len(self.tree)-len(leaves):]:
                these_possible_tree.append(node[0])
                
            pred_tree = {}
            for i in range(len(self.tree)):
                pred_tree[self.post_dict[i]] = these_possible_tree[i]
            possible_leaves.append(pred_tree)
            
            
            self.leaves = []
            self.prefix_table = defaultdict(list)
            self.children = defaultdict(list)
            self.prefixes = defaultdict(list)
            self.tree = [[]]* (2*(2**const.LEVEL)-1)  
        return possible_leaves
     
     
     
import visualizetree




tracks = []
scores = []
not_succ = 0
for rep in range(1):
    
    fw = forward.FORWARD()
    orig_tree = fw.genTree()
    interval_nodes, these_leaves = fw.getSeps()
        
    gj = GREEDYJOIN()
    preds = gj.rungreedyjoin(these_leaves.values(),const.UPPER_BOUND)        
    ev = evaluate.Evaluate()
    
    max_score = 0
    max_tree_ev = []
    '''
    for t in preds:
        score = ev.run_evaluate(orig_tree, t)
        if score > max_score:
        #if t['p']==const.ROOT[0] and score > max_score:
            max_score = score
            max_tree_ev = t
    #print "==================================================="       
    #print visualizetree.list_from_dict(max_tree,gj.post_dict)
    print max_score        
    '''
    likelihood = float("-inf")
    max_tree = {}
    for t in preds:
        prob = gj.calculate_tree_probs(visualizetree.list_from_dict(t,gj.post_dict))
        #print prob
        if prob > likelihood:
            max_tree = t
            likelihood = prob
    print max_tree
    print "Final" ,  ev.run_evaluate(orig_tree, max_tree)    
    '''
    if sc == 0:
        #print visualizetree.list_from_dict(max_tree_ev,gj.post_dict)
        not_succ+=1
    #visualizetree.run_all(orig_tree, max_tree, 'temp', 'tempp')
    print likelihood
    if len(max_tree) > 0 :
        gj.tree1=orig_tree
        gj.tree2=max_tree
        tracks.append(gj.tree_eq('p','p'))
        scores.append(sc)
    #print max_tree
    
print tracks
print sum(scores)/len(scores)
print not_succ
'''