from difflib import SequenceMatcher
import needle
from collections import defaultdict
import treeutils
import const
import random
import math
class GREEDYJOIN():
    def __init__(self):
        self.leaves = []
        self.prefix_table = defaultdict(list)
        self.children = defaultdict(list)
        self.prefixes = defaultdict(list)
        self.tree = [[]]* (2*(2**const.LEVEL)-1)

        
    def find_common_start_and_end(self, seq1, seq2):
        s = SequenceMatcher()
        s.set_seq1(seq1)
        s.set_seq2(seq2)
        matches = s.get_matching_blocks()
        if len(matches) == 2 and len(seq1) <= len(seq2) :
            prefix = seq1
            suffix = ""
            middle = ""
        elif len(matches) == 2 and len(seq1) > len(seq2):
            prefix = seq2
            suffix = ""
            middle = ""
        elif len(matches) == 3:
            prefix = seq1[matches[0][0]:matches[0][2]]
            suffix = seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]
            middle = ""
        elif len(matches) == 4:
            prefix = seq1[matches[0][0]:matches[0][2]]
            suffix = seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]            
            middle = seq1[matches[1][0]:matches[1][0]+matches[1][2]]
    
        return prefix, suffix, middle
    
    def find_common_start(self, seq1, seq2):
        s = SequenceMatcher()
        s.set_seq1(seq1)
        s.set_seq2(seq2)
        matches = s.get_matching_blocks()
        prefix = seq1[matches[0][0]:matches[0][2]]
        #+seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]   
        return prefix
            
    def find_common_blocks(self, seq1, seq2):
        blocks = needle.needle(seq1, seq2).split(" ")
        return blocks

            
        
    def pair_seqs(self, this_level_num):
        these_prefixes = self.prefixes[this_level_num]
        rng = range(len(these_prefixes))
        self.prefix_table[this_level_num] = [[[] for i in rng] for j in rng]
        for i in range(len(these_prefixes)):
            for j in range(i,len(these_prefixes)):
                if i == j :
                    self.prefix_table[this_level_num][i][j]= ""
                else:
                    self.prefix_table[this_level_num][i][j] = self.find_common_start(these_prefixes[i], these_prefixes[j])
                    self.prefix_table[this_level_num][j][i] = self.find_common_start(these_prefixes[i], these_prefixes[j])

    def greedy_pairs(self,this_level_num):
        max_len = -1
        pairs = []
        indexes = range(2**this_level_num)
        ending = (len(indexes)/2) 
        while len(pairs) < ending :
            max_len = -1
            for i in range(len(indexes)-1):
                for j in range(i+1, len(indexes)):                    
                    if len(self.prefix_table[this_level_num][indexes[i]][indexes[j]]) > max_len:
                        max_len = len(self.prefix_table[this_level_num][indexes[i]][indexes[j]])                 
                        max_pair = [indexes[i],indexes[j]]
            to_be_removed1 = max_pair[0]
            to_be_removed2 = max_pair[1]
            indexes.remove(to_be_removed1)
            indexes.remove(to_be_removed2)
            pairs.append(max_pair)
        for p in pairs:
            self.prefixes[this_level_num-1].append(self.prefix_table[this_level_num][p[0]][p[1]])
            self.children[this_level_num-1].append((p[0],p[1]))
        


    def greedy_pairs_all(self):
        for this_level_num in range(const.LEVEL, 0, -1):
            self.pair_seqs(this_level_num)
            self.greedy_pairs(this_level_num)
            
        self.feel_tree()
        
    def fill_children(self, k, m):
        child_level = treeutils.get_layer_fromtreeid(k)
        if child_level < const.LEVEL:
            these_children = self.children[child_level][m]
            self.tree[2*k+1] = (self.prefixes[child_level+1][these_children[0]],these_children[0])
            self.tree[2*k+2] = (self.prefixes[child_level+1][these_children[1]],these_children[1])   
            self.fill_children(2*k+1, these_children[0])
            self.fill_children(2*k+2, these_children[1])           
        
        
    def feel_tree(self):        
        self.tree[0] = (self.prefixes[0][0],0)
        self.fill_children(0,0)
        #print self.tree[-8:]
            
    
    def njprocess(self, leaves):
        self.prefixes[const.LEVEL] = leaves
        self.greedy_pairs_all()

    def rungreedyjoin(self, leaves, rounds):
        possible_leaves = []
        for perm in range(rounds):
            leaves_ids = range(len(leaves))
            l = list(zip(leaves, leaves_ids))
            random.shuffle(l)
            shuffled_leaves, shuffled_ids = zip(*l)
            iddict = {}
            for i in range(len(leaves)):
                iddict[i] = shuffled_ids[i]
            self.njprocess(shuffled_leaves)
            these_possible_leaves = []
            for node in self.tree[-len(leaves):]:
                these_possible_leaves.append(iddict[node[1]])
            possible_leaves.append((these_possible_leaves, math.log(0.5,2)))
            
            self.leaves = []
            self.prefix_table = defaultdict(list)
            self.children = defaultdict(list)
            self.prefixes = defaultdict(list)
            self.tree = [[]]* (2*(2**const.LEVEL)-1)  
        return possible_leaves
'''
these_leaves = {'p000': 'GGCCCAGACTGAGCACGCCTATGA', 'p001': 'GGCCCAGACTGAGCACGCGACTGA', 'p011': 'GGCCCAGACTGAGCACGACTATGA', 'p010': 'GGCCCAGACTGAGCACGACGTGA', 'p101': 'GGCCCAGACTGAGCACGCGGGACTGA',  'p100': 'GGCCCAGACTGAGCACGCGGTGA', 'p110': 'GGCCCAGACTGAGCACGCTAAATGA', 'p111': 'GGCCCAGACTGAGCACGCTATGA'}

gj = GREEDYJOIN()
print gj.rungreedyjoin(these_leaves.values(), 5)        
'''