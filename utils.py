import numpy as np
from string import *
import random


def logSumExp(listoflogs):
    if len(listoflogs) == 1:
        #print listoflogs
        return listoflogs[0]
    listoflogs = np.array(listoflogs)
    maxxlog = np.max(listoflogs)
    if maxxlog == float('-inf') and not np.isnan(np.min(listoflogs)):
        return listoflogs[0]
    elif np.isnan(np.min(listoflogs)):
        print listoflogs
    diffs = listoflogs - maxxlog
    sumOfExp = np.exp(diffs).sum()
    if np.isinf(sumOfExp):
        print listoflogs
    
    #print maxxlog + np.log(sumOfExp)
    return maxxlog + np.log(sumOfExp)
'''
This function returns to values for cae of match or mismatch
'''
def diagonal(n1,n2,pt):
    if(n1 == n2):
        return pt['MATCH']
    else:
        return pt['MISMATCH']

'''
This function gets the optional elements of the aligment matrix and returns the elements for the pointers matrix.
'''
def pointers(di,ho,ve):

    pointer = max(di,ho,ve) #based on python default maximum(return the first element).

    if(di == pointer):
        return 'D'
    elif(ho == pointer):
        return 'H'
    else:
         return 'V'

def NW(s1,s2,match = 5,mismatch = -10, gap = 1):
    penalty = {'MATCH': match, 'MISMATCH': mismatch, 'GAP': gap} #A dictionary for all the penalty valuse.
    n = len(s1) + 1 #The dimension of the matrix columns.
    m = len(s2) + 1 #The dimension of the matrix rows.
    al_mat = np.zeros((m,n),dtype = int) #Initializes the alighment matrix with zeros.
    p_mat = np.zeros((m,n),dtype = str) #Initializes the alighment matrix with zeros.
    #Scans all the first rows element in the matrix and fill it with "gap penalty"
    for i in range(m):
        al_mat[i][0] = penalty['GAP'] * i
        p_mat[i][0] = 'V'
    #Scans all the first columns element in the matrix and fill it with "gap penalty"
    for j in range (n):
        al_mat[0][j] = penalty['GAP'] * j
        p_mat [0][j] = 'H'
    #Fill the matrix with the correct values.

    p_mat [0][0] = 0 #Return the first element of the pointer matrix back to 0.
    for i in range(1,m):
        for j in range(1,n):
            di = al_mat[i-1][j-1] + diagonal(s1[j-1],s2[i-1],penalty) #The value for match/mismatch -  diagonal.
            ho = al_mat[i][j-1] + penalty['GAP'] #The value for gap - horizontal.(from the left cell)
            ve = al_mat[i-1][j] + penalty['GAP'] #The value for gap - vertical.(from the upper cell)
            al_mat[i][j] = max(di,ho,ve) #Fill the matrix with the maximal value.(based on the python default maximum)
            p_mat[i][j] = pointers(di,ho,ve)
    print np.matrix(al_mat)
    print np.matrix(p_mat)
    print al_mat[-1][-1]
    

def calcDeleteProb(leng):
    ins_pos_prob = [0.1 / (leng - 2)] * leng
    ins_pos_prob[120] = 0.45
    ins_pos_prob[121] = 0.45
    return ins_pos_prob


def calcInsertProb(leng):
    ins_pos_prob = [0.1 / (leng - 2)] * leng
    ins_pos_prob[128] = 0.45
    ins_pos_prob[129] = 0.45
    return ins_pos_prob
    
def shuffle_leaves_forNJ(leaves, info_mat):
    random.shuffle(leaves)
    infomat_shuffled_rows = []
    infomat_shuffled = []
    for row in info_mat:
        shuffled_row = [ row[i] for i in leaves]
        infomat_shuffled_rows.append(shuffled_row)
        
    infomat_shuffled = [ infomat_shuffled_rows[j] for j in leaves]
    
    return (leaves, infomat_shuffled)
    
seq1 = "ATCCCAGGGAAACGCCCATGCAATTAGTCTATTTCTGCTGCAAGTAAGCATGCATTTGTAGGCTTGATGCTTTTTTTCTGCTTCTCCAGCCCTGGCCTGGGTCAATCCTTGGGGCCCAGACTGAGCACGCTTTGGTGATGGCAGAGGAAAGGAAGCCCTGCTTCCTCCAGAGGGCGTCGCAGGACAGCTTTTCCTAGACAGGGGCTAGTATGTGCAGCTCCTGCACCGGGATACTGGTTGACAAGTTTGGCTGGG"
seq2 = "ATCCCAGGGAAACGCCCATGCAATTAGTCTATTTCTGCTGCAAGTAAGCATGCATTTGTAGGCTTGATGCTTTTTTTCTGCTTCTCCAGCCCTGGCCTGGGTCAATCCTTGGGGCCCAGACGCAGAGGAAAGGAAGCCCTGCTTCCTCCAGAGGGCGTCGCAGGACAGCTTTTCCTAGACAGGGGCTAGTATGTGCAGCTCCTGCACCGGGATACTGGTTGACAAGTTTGGCTGGG"


