import sys
import getopt
import numpy as np
import collections
import math
import const 
It = collections.Iterable
#letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
#letters = ['p1110', 'p1111', 'p1101', 'p1100', 'p1011', 'p1010', 'p1000', 'p1001', 'p0111', 'p0110', 'p0001', 'p0000', 'p0010', 'p0011', 'p0100', 'p0101']
#letters = ['p10', 'p11', 'p01', 'p00']
#letters = ['p110', 'p111', 'p000', 'p001', 'p011', 'p010', 'p101', 'p100']
letters = []

digits = range(100000,200000)
node_dict = dict()


def groups(L,N=3):
    R = range(0,len(L),N)
    return [L[i:i+N] for i in R]


def one_round(A,otus, thresh, level, count):
    #print "OTUs:"
    #print otus
    #print "Distance matrix:"
    #print A
    #print
    #print "Column sum:"
    div = np.sum(A,axis=1) #"divergence" matrix
    #print div
    n = A.shape[0] #get number of rows
    #print n
    # two nodes only:  we're done
    if n == 2:
        dist = A[1][0]
        nD = node_dict[otus[0]]
        nD['up'] = otus[1]
        nD['d_up'] = dist
        return None,otus,thresh,level 

    #Find the i,j to work on using divergence:
    #based on a distance matrix relating the r taxa, calculate Q-matrix and 
    #find the pair of taxa for which Q(i,j) has its lowest value. 
    i,j = 0,0
    low_value = A[i][j]
    for r,row in enumerate(A):
        #print row 
        if r == 0:  continue
        for c,col in enumerate(row):
        #print col
            if  r < thresh and c < thresh:
                if c >= r:  continue
                dist = A[r][c] 
                #print dist
                first = div[c]
                #print first
                second = div[r]
                #print second
                correction = (first + second)/(n-2)
                value = dist - correction
                #print r, c, dist, first, second, correction, value
                if value < low_value  :
                    i,j,low_value = r,c,value
    thresh = thresh - 2
    if thresh == 0:
        level  = level - 1
        thresh = 2**level
    #Merge i and j entries
    #Calculate distance of new node from tips
    new_name = digits[count]
    #print
    #print 'merge:', i, otus[i], j, otus[j], 'to', new_name
    
    #Dist from node[i]
    dist =  A[i][j]
    ### My Change
    ### TODO
    diff = div[i] - div[j]
    #print 'orig dist', dist, 'div diff', diff
    dist_i = (dist/2.0 + diff/(2*(n-2))) 
    dist_j = (dist - dist_i) 
    

    
    #print dist_i, dist_j
    node = { 'L':otus[i], 'dL':dist_i, #"L"&"R" state for "Left" and "Ridht"
             'R':otus[j], 'dR':dist_j } #"dL" & "dR" are left/right distances from divergence point to the left/right node. 
    node_dict[new_name] = node
    #print node
    
    #Calculate distances to new node,
    # i,j assigned above
    tL = list()
    ij_dist = A[i][j]
    for k in range(len(A[0])):
        if k == i or k == j:  continue
        #print 'node', otus[k], A[i][k], A[j][k], ij_dist,
        ### My Change
        ### TODO
        if k < thresh:
            coef = 2
        else: 
            coef = 1
        dist = coef*(A[i][k] + A[j][k] - ij_dist)/2
        #print dist
        tL.append(dist)
    #print 'to new node:', tL
    
    #print
    #print A
    #print

    #Remove columns and rows involving i or j
    if i < j:  i,j = j,i
    assert j < i
    #print 'i', i, 'j', j
    sel = range(n)
    for k in [j,i]:    #Larger first
        sel.remove(k)
        #print 'sel', sel
        A1 = A[sel,:]
        A2 = A1[:,sel]
        #print A2
    A = A2
    #print
    #Correct the otu names:
    otus =  otus[:j] + otus[j+1:i] + otus[i+1:] + [new_name] 
    
    #Add col at left and row at top for new node
    new_col = np.array(tL)
    new_col.shape = (n-2,1)
    A = np.hstack([A,new_col])

    new_row = np.array([0] + tL)
    new_row.shape = (1,n-1)
    A = np.vstack([A,new_row])
    #print
    #print A, otus, thresh , level
    return (A,otus,thresh,level)
    

def build_tree(dist_mat, level, these_keys) :
  global letters
  letters = these_keys
  N = len(dist_mat)
  A = dist_mat
  otus = list(letters[:N])
  A = np.array(A)
  A.shape = (N,N)
  #print A

  count = 0
  thresh = 2**level
  while True:
    #print 'round', count
    #print A,otus,thresh,level
    A,otus,thresh,level = one_round(A,otus,thresh,level,count)
    #print A
    if A is None:  break
    count += 1
    #print A
    #print
  #print


  #print "Printing final results:"
  #print
  kL = ['L','dL','R','dR','up','d_up'] 
  #print node_dict
  '''
  for node in sorted(node_dict.keys()):
    print node, ':   ',
    for k in kL:
        nD = node_dict[node]
        if not k in nD:
            continue
        v = nD[k]
        if k in ['dL','dR']:
            v = '%3.3f' % v
        print v, ' ',
    print    
  '''
  return node_dict


