LEVEL = 4;
INIT_LENGTH = 20;

INS_PROB = 0.70
DEL_PROB = 0.15
NO_PROB = 0.15

DEL_DIR_PRB = {"L" : 0.5 , "R" : 0.5}
#NUCL_PRO_INS = [0.15, 0.55, 0.15, 0.15]
NUCL_PRO_INS = [0.25, 0.25, 0.25, 0.25 ]

#NUCL_PRO_INS_DICT = {"A" :0.25, "C":0.25, "G":0.25, "T":0.25, "N":0.25}
#NUCL_PRO_DEL_DICT = {"A" :0.25, "C":0.25, "G":0.25, "T":0.25, "N":0.25}

NUCL_PRO_INS_DICT = {"A" :1, "C":1, "G":1, "T":1, "N":1}
NUCL_PRO_DEL_DICT = {"A" :1, "C":1, "G":1, "T":1, "N":1}

ROOT = ["GGCCCAGACTGAGCACGTGA"]

INS_POS_PRB = {-3: 0.95, -4: 0.05 }
#INS_POS_PRB = {-3: 1.00 }

DEL_POS_PRB = {-3: 0.70 , -4 : 0.30}

INS_LEN_PRB = {1: 0.25, 2: 0.20, 3: 0.40 , 4: 0.15}
DEL_LEN_PRB = {1: 0.30 , 8: 0.50, 2: 0.20}

KEYS = []
UPPER_BOUND = 40
IMPT_SAMP = 7

#ALL: all permutations 
#NJ1 : nj with probs
#NJ2 : nj with dists
#"PREFIXNJ" : use common prefix length to find the structure
LEAVES_CHOICE = "PREFIXNJ" 


## These are the weights that can be used in evaluation
## Let's decide about the values later
## First number goes for coming from the same root
## And then it goes to the next generations

EVALUATION_METRIC = [2,2,1,2,1]