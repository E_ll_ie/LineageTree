import subprocess
import const


def write_dot_file(tree1, tree2, outf):
    temp_1= "digraph G { graph [center=true, rankdir=LR]; ranksep=0.3; nodesep=0.3; node [color=goldenrod2, style=\"filled,solid\" , fontsize=44, fontname=Courier,shape=box]; edge [color=Black,size=0.5, penwidth=5];"
    temp_2= "digraph G {graph [center=true, rankdir=LR];  ranksep=0.3; nodesep=0.3; node [color=aquamarine3, style=filled, fontsize=44, fontname=Courier,shape=box]; edge [color=Black,size=0.5, penwidth=5];" 
    for ind in range(2*(2**const.LEVEL) -1 ):
        temp_1+= str(ind)+"[label="+ tree1[ind]+"];"
        temp_2+= str(ind)+"[label="+ tree2[ind]+"];"
    for ind in range(2**const.LEVEL -1 ):
        '''
        temp_1+=tree1[ind]+str(ind)+ " -> "+ tree1[2*ind+1]+""+str(2*ind+1) +"; "
        temp_1+=tree1[ind]+""+str(ind)+ " -> "+ tree1[2*ind+2]+""+str(2*ind+2) +"; "

        temp_2+=tree2[ind]+" "+str(ind)+ " -> "+ tree2[2*ind+1]+" "+str(2*ind+1) +"; "
        temp_2+=tree2[ind]+" "+str(ind)+ " -> "+ tree2[2*ind+2]+" "+str(2*ind+2) +"; "
        '''
        temp_1+=str(ind)+ " -> "+ str(2*ind+1) +"; "
        temp_1+=str(ind)+ " -> "+ str(2*ind+2) +"; "

        temp_2+=str(ind)+ " -> "+ str(2*ind+1) +"; "
        temp_2+=str(ind)+ " -> "+ str(2*ind+2) +"; "
        
    temp_1 += "}"
    temp_2 += "}"
    open(outf+"_tree1.dot",'w').write(temp_1)
    open(outf+"_tree2.dot",'w').write(temp_2)

    
def run_graphviz(outf, outp):
    subprocess.call([r"C:\Users\Elmira\Downloads\graphviz-2.38\release\bin\dot.exe","-Tpng",outf+"_tree1.dot","-o",outp+"_tree1.png"])
    subprocess.call([r"C:\Users\Elmira\Downloads\graphviz-2.38\release\bin\dot.exe","-Tpng",outf+"_tree2.dot","-o",outp+"_tree2.png"])
    


def gen_postx(t_size):
    post_dict = {}
    post_dict[0] = "p"
    for m in range(1,t_size):
        if m %2 ==0:
            parent = (m-2)/2
            postx = post_dict[parent]+"1"
        else:
            parent = (m-1)/2
            postx = post_dict[parent]+"0"
        post_dict[m] = postx
    return post_dict


def list_from_dict(tree_dict, post_dict):
    t_size = 2*(2**const.LEVEL)-1
    tree_list = []
    for ind in range(t_size):
        tree_list.append(tree_dict[post_dict[ind]])
    return tree_list        
        
def run_all(treed1, treed2, dotf, pngf):
    post_dict = gen_postx(2*(2**const.LEVEL)-1)
    tree1 = list_from_dict(treed1, post_dict)
    tree2 = list_from_dict(treed2, post_dict)
    write_dot_file(tree1,tree2, dotf)
    run_graphviz(dotf, pngf)
    
    
    
    

    
