## -*- coding: utf-8 -*-
import random
import numpy as np
import const
from bisect import bisect
from random import random as rnd
import logging

#random.seed(118)
#np.random.seed(118)
class FORWARD:
    def __init__(self):
        #self.SEQ = "".join([random.choice('ACGT') for _ in range(40)])
        self.SEQ = const.ROOT[0]
        self.SEQS = {'p': self.SEQ}
        self.INTM = {'p' : self.SEQ}
        self.LVS = {}
        self.BLOCKED = []
        self.logger = logging.getLogger(__name__)
        #logging.basicConfig(level=logging.WARNING)
        self.logger.setLevel(logging.WARNING)
    
    def WeightedACGT(self):
        chars = 'ACGT'
        weights = const.NUCL_PRO_INS
        breakpoints = [sum(weights[:x+1]) for x in range(4)]
        #print chars[bisect(breakpoints, rnd())]
        return chars[bisect(breakpoints, rnd())]
        
    def makeChildren(self,parent, leaf):
        seq = [x for x in self.SEQS.get(parent)]
        for i in range(2):
            if parent in self.BLOCKED:
                new_seq = seq
                self.BLOCKED.append(parent+str(i))
            else:
                ins_del_sel = np.random.choice(['insertion', 'deletion', 'no-change'], 1, p=[const.INS_PROB, const.DEL_PROB, const.NO_PROB])
                if ins_del_sel == 'insertion':
                    loc = len(seq) + np.random.choice(const.INS_POS_PRB.keys(), 1, p=const.INS_POS_PRB.values())[0]
                    ins_length = np.random.choice(const.INS_LEN_PRB.keys(), 1, p=const.INS_LEN_PRB.values())[0]
                    #ins_seq = [random.choice('ACGT') for _ in range(ins_length)]
                    ins_seq = [self.WeightedACGT() for _ in range(ins_length)]
                    new_seq = seq[0:loc] + ins_seq + seq[loc:len(seq) + 1]
                elif ins_del_sel == 'deleteion':
                    loc = len(seq) + np.random.choice(const.DEL_POS_PRB.keys(), 1, p=const.DEL_POS_PRB.values())[0]
                    del_length = np.random.choice(const.DEL_LEN_PRB.keys(), 1, p=const.DEL_LEN_PRB.values())[0]
                    del_dir = np.random.choice(const.DEL_DIR_PRB.keys(), 1, p=const.DEL_DIR_PRB.values())[0]
                    if del_dir == "R":
                        new_seq = seq[0:loc] + seq[len(seq)+loc + del_length:len(seq) + 1]
                    else: 
                        new_seq = seq[0:loc - del_length] + seq[loc:len(seq)+1]
                    if len(new_seq) < 16 or loc+del_length > len(seq):
                        self.BLOCKED.append(parent+str(i))
                else:
                    new_seq = seq
                    
            self.SEQS[parent + str(i)] = "".join(new_seq)
            if leaf:
                self.LVS[parent + str(i)] = "".join(new_seq)
            else:
                self.INTM[parent + str(i)] = "".join(new_seq) 
    

    
    def getSeps(self):
        return self.INTM, self.LVS
        
    def getSEQS(self):
        return self.SEQS
    
    def genTree(self):
        for i in range(const.LEVEL - 1):
            for key in self.SEQS.keys():
                if len(key) == i + 1:
                    self.makeChildren(key, False)
            #print 'level ' + str(i) + "  is generated"
        i = const.LEVEL -1 
        for key in self.SEQS.keys():
            if len(key) == i + 1:
                self.makeChildren(key, True)
        self.logger.info("level %d  is generated", i)
        ###print self.SEQS
        return self.SEQS




