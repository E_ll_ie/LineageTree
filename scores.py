import math
import utils
from difflib import SequenceMatcher
import const 
import njbalanced
from collections import defaultdict

class SCORE:
    def __init__(self):
        self.orderedleaves = []
        self.info_mat = []
        self.tree_dict = defaultdict(dict)
        
    def change_prob_to_dist(self, info_mat):
        rng = range(len(info_mat))
        score_mat  = [[[] for i in rng] for j in rng]
        for rowind in rng :
            for colind in rng:
                #print info_mat[rowind][colind]
                if len(info_mat[rowind][colind]) == 0 :
                    score_mat[rowind][colind] = 10**30
                    score_mat[colind][rowind] = 10**30
                else:
                    score_mat[rowind][colind] = -1 * utils.logSumExp([t[1] for t in info_mat[rowind][colind]])
                    #score_mat[rowind][colind] = math.log(1/max(info_mat[rowind][colind],key=lambda item:item[1])[1],2)
                    score_mat[colind][rowind] = score_mat[rowind][colind]
        return score_mat
        
    def test_calculating_seq_distance(self, this_layer):
        s = SequenceMatcher()
        rng = range(len(this_layer))
        score_mat  = [[[] for i in rng] for j in rng]
        for this_node in rng:
            for that_node in rng:
                s.set_seq1(this_layer[this_node])
                s.set_seq2(this_layer[that_node])
                #try_this =  s.get_opcodes()[0][2]
                #if try_this != 0 :
                if s.ratio() != 0 :
                    #score_mat[this_node][that_node] = 1.0/try_this
                    score_mat[this_node][that_node] = 1/s.ratio()
                else:
                    score_mat[this_node][that_node] = 1000
        #print score_mat
        return score_mat
    
    def extract_leaves(self,this_node):
        if this_node not in self.tree_dict:
            self.orderedleaves.append(this_node)
        else:
            self.extract_leaves(self.tree_dict[this_node]['L'])
            self.extract_leaves(self.tree_dict[this_node]['R'])
            
    
    def run_njbalanced_dist(self, this_layer, permutations):
        score_mat = self.test_calculating_seq_distance(this_layer)
        orderedleaves_list  = []
        leaves_keys = range(len(this_layer))
        for i in range(permutations):
            #print i, self.tree_dict
            this_leaves_keys, this_score_mat  = utils.shuffle_leaves_forNJ(leaves_keys, score_mat)
            self.tree_dict = njbalanced.build_tree(this_score_mat, const.LEVEL, this_leaves_keys)
            finalone = max(self.tree_dict.keys())
            finaltwo = finalone -1 
            self.tree_dict['root'] = {'R' : finalone, 'L' : finaltwo}
            self.extract_leaves('root')
            orderedleaves_list.append((self.orderedleaves,math.log(0.5,2)))
            self.orderedleaves = []
            for key in self.tree_dict.keys():
                del self.tree_dict[key]
            self.tree_dict = defaultdict(dict)
        #print  orderedleaves_list      
        return orderedleaves_list        
        
        
    def run_njbalanced_prob(self, leaves_keys, permutations):
        score_mat = self.change_prob_to_dist(self.info_mat)
        orderedleaves_list  = []
        leaves_keysl = []
        for leaf in leaves_keys:
            leaves_keysl.append(leaf)
        for i in range(permutations):
            #print i, self.tree_dict
            this_leaves_keys, this_score_mat  = utils.shuffle_leaves_forNJ(leaves_keysl, score_mat)
            self.tree_dict = njbalanced.build_tree(this_score_mat, const.LEVEL, this_leaves_keys)
            finalone = max(self.tree_dict.keys())
            finaltwo = finalone -1 
            self.tree_dict['root'] = {'R' : finalone, 'L' : finaltwo}
            self.extract_leaves('root')
            orderedleaves_list.append((self.orderedleaves,math.log(0.5,2)))
            self.orderedleaves = []
            for key in self.tree_dict.keys():
                del self.tree_dict[key]
            self.tree_dict = defaultdict(dict)
        return orderedleaves_list
        

        