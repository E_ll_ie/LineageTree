import const
from collections import defaultdict
import math
import forward
import numpy as np 
import random
import Nutils
import treeutils
import sequtils
import probcalculator as prbc
import parents
import utils
import sys
import scores
import evaluate
import greedyjoin
import visualizetree
import cProfile
import logging

'''
This function is supposed to get two sequence
Find all possible merging cases and their probabilities.
'''  
#random.seed(8)
#np.random.seed(8)
class RECONSTRUCT:
    def __init__(self):
        self.init_layers = defaultdict(list)
        self.COMMON_PARENTS_DICT = defaultdict(list)
        self.leaves = []
        self.dict_layers = defaultdict(dict)
        self.prbs_tables =  defaultdict(dict)
        self.t_size = 2*(2**const.LEVEL)-1
        self.tree_locs_probs = defaultdict(dict)
        self.post_dict = {}
        self.PRED_TREE = {}
        self.init_parent_dict = defaultdict(dict)
        self.possible_leaves = []
        # Create the Logger
        
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.ERROR)
        
        
        
    # Loading the leaves from a list
    def fill_leaves(self, given_leaves):
        self.leaves = given_leaves
        self.init_layers[const.LEVEL] = given_leaves
        

    ############################################################################################################################
    # Recursively genererates all possible combination of pairs  of leaves
    # This one doesn't keep the probabilities
    def recursive(self, a, b):
        n = len(a)
        if n == 0 or n == 1:
            bt = [j for i in b for j in i]
            self.possible_leaves.append(bt)
            return
        for i in range(1,n):
            this_one = [item for item in a if item not in [a[0],a[i]]]
            that_one  = b + [[a[0],a[i]]]
            #print a[i],a[0]
            if len(self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]) > 0 :
                self.recursive(this_one, that_one)
            
    # Recursively genererates all possible combination of pairs  of leaves
    # This one keeps the probabilities
    # If A and B have more than one possible parent, we first calculate the sum 
    # (This parent or that parent or the other one: hence the sum)
    # And then we get the product of porbs for all pairs
    # Of course all of this happens in log world.
    def recursive_withprobs(self, a, b , prob):
        n = len(a)
        if n == 0 or n == 1:
            bt = [j for i in b for j in i]
            self.possible_leaves.append((bt,prob))
            return
        for i in range(1,n):
            this_one = [item for item in a if item not in [a[0],a[i]]]
            that_one  = b + [[a[0],a[i]]]
            
            #print a[i],a[0]
            if len(self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]) > 0 :
                self.recursive_withprobs(this_one, that_one, prob)
                prob += utils.logSumExp([t[1] for t in self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]])
    
    ############################################################################################################################    
    # Loops over the generation, starting from the leaves
    # keeps all possible cases
    def gen_layers(self):
        for layer in range(const.LEVEL, 0, -1):
            #print layer
            self.gen_pre_layer(self.init_layers[layer], layer)   
            
        self.dict_layers = treeutils.encode_to_dict(self.init_layers)
        self.post_dict = treeutils.gen_postx(self.t_size)

        
    # Obviously generate possible seqs for previous layer
    def gen_pre_layer(self,this_layer, this_layer_num):
        init_pre_layer = set()
        node_num = 0
        for this_node in this_layer:
            generated_parents = parents.gen_parents(this_node)
            self.init_parent_dict[this_layer_num][node_num] = generated_parents
            init_pre_layer.update(generated_parents)
            node_num+=1
        # After generating the possible parents for each node, try to find those that
        # Can parent at least two nodes        
        self.gen_pre_layer_commons(self.init_parent_dict[this_layer_num], this_layer_num)
        
        ## This is the part I wanted to use to fill some of the "N"s
        ## Not sure if it's worth the increased running time though
        '''
        updated_pre_layer = init_pre_layer
        self.init_layers[this_layer_num-1]= updated_pre_layer
        eliminations, additions  = Nutils.readjust_deletions(updated_pre_layer, this_layer)
        updated_pre_layer.update(additions)        
        self.init_layers[this_layer_num-1]= list(updated_pre_layer.difference(set(eliminations)))
        '''
        
    # For parents generated in each layer, 
    # Generate another list of common parents
    # So we can keep track of those parents that can really parent two children
    def gen_pre_layer_commons(self, init_prev_layer, this_layer_num):
        nodes = init_prev_layer 
        # For now only for the leaves I keep all the probabilities
        if this_layer_num == const.LEVEL :
            self.COMMON_PARENTS_DICT[this_layer_num] = [[[] for i in range(2**this_layer_num)] for j in range(2**this_layer_num)]

        for this_ind in range(len(nodes)):
            for that_ind in range(this_ind+1, len(nodes)): 
                ## TODO Define this dictionary again I guess
                ## With id, layer and sequence
                this_common_parents = []
                this_common_parents = parents.compare_parents_withPRB(nodes[this_ind],nodes[that_ind])
                this_common_parents = parents.sumprobs_common_parents(parents.compare_parents_withPRB(nodes[this_ind],nodes[that_ind]))
                temp = set()
                for p in this_common_parents:
                    temp.add(p[0])
                self.init_layers[this_layer_num - 1] += temp 
                if this_layer_num == const.LEVEL:                                                          
                    self.COMMON_PARENTS_DICT[this_layer_num][this_ind][that_ind]  = list(set(this_common_parents))            
                    self.COMMON_PARENTS_DICT[this_layer_num][that_ind][this_ind]  = list(set(this_common_parents))  
        
        
        self.init_layers[this_layer_num - 1] =  list(set(self.init_layers[this_layer_num - 1]))
        if this_layer_num == const.LEVEL:
            if const.LEAVES_CHOICE == "ALL":
                self.recursive_withprobs(range(len(nodes)), [], 0)
            elif const.LEAVES_CHOICE == "NJ1":
                sc = scores.SCORE()
                sc.info_mat = self.COMMON_PARENTS_DICT[const.LEVEL]
                self.possible_leaves =  sc.run_njbalanced_prob(range(len(self.leaves)),const.UPPER_BOUND)
                
            elif const.LEAVES_CHOICE == "NJ2" :
                sc = scores.SCORE()
                self.possible_leaves = sc.run_njbalanced_dist(self.init_layers[this_layer_num],const.UPPER_BOUND)
                
            elif const.LEAVES_CHOICE == "PREFIXNJ" :
                gj = greedyjoin.GREEDYJOIN()
                self.possible_leaves = gj.rungreedyjoin(self.init_layers[this_layer_num], const.UPPER_BOUND)
                
        self.logger.info(" %s of layers, have a set of possible sequences right now", str(const.LEVEL - this_layer_num) )
     ############################################################################################################################   

    # The fun part of course.
    # After having a set for each layer
    # For each layer and the next one calculates all probabilities        
    def calc_prbs(self):
        for layer in range(0, const.LEVEL):
            for parents_id in self.dict_layers[layer]:
                for child_id in self.dict_layers[layer+1]:
                    self.prbs_tables[layer][(parents_id, child_id)] = \
                    prbc.calc_prbXY(self.dict_layers[layer][parents_id],self.dict_layers[layer+1][child_id] )
                    #if np.isnan(self.prbs_tables[layer][(parents_id, child_id)]):
                    #    print layer, parents_id, child_id,self.prbs_tables[layer][(parents_id, child_id)]
            
    
        
    # Calculate the probability of seeing each seq in each position of tree
    # In a recursive manner
    def Prob(self, pos_in_tree, start_node):
        layer = treeutils.get_layer_fromtreeid(pos_in_tree)
        #print start_node, layer
        if layer == const.LEVEL:
            return 0
        else:
            probl = 0
            problist = []
            if layer+1 == const.LEVEL:
                # Don't forget to set the id in the leaves level as this
                layer_to_look = [self.leaves[2*pos_in_tree+1 - 2**(const.LEVEL) + 1]+1] 
                layer_to_look_other = [self.leaves[2*pos_in_tree+2 - 2**(const.LEVEL) + 1]+1] 
            else: 
                layer_to_look = self.dict_layers[layer+1] 
                layer_to_look_other = self.dict_layers[layer+1]
            for child1_id in layer_to_look:                        
                for child2_id in layer_to_look_other:
                    if (layer+1, child1_id) in self.tree_locs_probs[2*pos_in_tree+1]:
                        child1_prob = self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)]
                    else:
                        self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)] = self.Prob(2*pos_in_tree+1,child1_id)
                        child1_prob = self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)]
                        
                    if (layer+1, child2_id) in self.tree_locs_probs[2*pos_in_tree+2]:
                        child2_prob = self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)]
                    else:
                        self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)] = self.Prob(2*pos_in_tree+2,child2_id)
                        child2_prob = self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)]                        
                    
                    #Lets move to log domain 
                    problist += [child1_prob + self.prbs_tables[layer][(start_node, child1_id)] + child2_prob + self.prbs_tables[layer][(start_node, child2_id)]]
            
            probl = utils.logSumExp(problist)

            return probl
        
        
        
    # Calculate the probabilities of seeing each sequence in each    
    def reconstruct(self, partition):
        possible_trees = []
        probs = []
        possible_roots = []
        self.possible_leaves.sort(key=lambda x: x[1], reverse=True)
        # For each generated root like sequence, we calculate the probabilities
        self.logger.info("There are  %s permutations to check; " , str(len(self.possible_leaves)))        
        for r_id in self.dict_layers[0]:
            this_r= self.dict_layers[0][r_id]
            if sequtils.compare_seqs(this_r, const.ROOT[0]):
                possible_roots.append(r_id)
                
        self.logger.info("There are %s roots to check;" , str(len(possible_roots)))        
        self.logger.info("However, we are going through at most  %s of them" , str(const.UPPER_BOUND) )
        root_counter = 0

        for possible_root_id in possible_roots:
            possible_root = self.dict_layers[0][possible_root_id]
            self.logger.info(possible_root)
            # Here: Probs for this root
            root_counter +=1
            leaves_counter = 0
            for this_leaves_perms_tuple in self.possible_leaves[partition*const.UPPER_BOUND:(partition+1)*const.UPPER_BOUND]:
                this_leaves_perms = this_leaves_perms_tuple[0]
                tree = [""] * self.t_size
                nodeids = [[]]  * self.t_size
                self.leaves = this_leaves_perms
                leaves_counter+=1
                this_prob = self.Prob(0, possible_root_id)
                if this_prob > float('-inf'):
                    probs.append(this_prob)
                    tree[0] = possible_root
                    nodeids[0] = possible_root_id
                    for child_tpos in range(1, self.t_size):
                        parent_tpos = int(math.floor(float(child_tpos-0.001)/2))
                        this_parent_id = nodeids[parent_tpos]
                        this_layer = treeutils.get_layer_fromtreeid(child_tpos)
                        this_p = float('-inf')
                        for possible_child_id in self.dict_layers[this_layer]:
                            # FIXME you shouldn't need this
                            if (this_layer, possible_child_id) in self.tree_locs_probs[child_tpos]:
                                this_child = self.dict_layers[this_layer][possible_child_id]   
                                # Lets go to log domain
                                if self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                    self.prbs_tables[this_layer -1 ][(this_parent_id, possible_child_id)] > this_p \
                                    and len(this_child) > 0:
                                    this_p = self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                        self.prbs_tables[this_layer - 1][(this_parent_id, possible_child_id)]
                                    tree[child_tpos] = this_child
                                    nodeids[child_tpos] = possible_child_id
                                if this_p == float('-inf'):
                                    if self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                        self.prbs_tables[this_layer -1 ][(this_parent_id, possible_child_id)] > this_p :
                                        this_p = self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                            self.prbs_tables[this_layer - 1][(this_parent_id, possible_child_id)]
                                        tree[child_tpos] = this_child
                                        nodeids[child_tpos] = possible_child_id                           


                    possible_trees.append(tree)
                if leaves_counter % 20 == 0:
                    self.logger.info( "From Root %s ; %s is done " , str(root_counter), str(leaves_counter))
                self.tree_locs_probs = defaultdict(dict)
        if len(probs) > 0:
            self.logger.info("Maximum Likelihood Value %f" , max(probs))
            pred_tree = possible_trees[probs.index(max(probs))]

            for i in range(len(pred_tree)):
                if len(pred_tree[i]) == 0:
                    pred_tree[i] = "XXX"
                self.PRED_TREE[self.post_dict[i]] = pred_tree[i]
                
        elif len(self.possible_leaves) >= (partition+1)*(const.UPPER_BOUND) :
            self.logger.info("FYI: We're going another round with other permutation of leaves")
            self.reconstruct(partition+1)

        
succ = []
not_predicted = []
with_error = []
for imp_sampling in range(5,15):
    this_succ = 0
    this_not_predicted = 0
    this_with_error = 0
    const.IMPT_SAMP = imp_sampling
    for k in range(40):
        fw = forward.FORWARD()
        rc = RECONSTRUCT()
        orig_tree = fw.genTree()
        interval_nodes, these_leaves = fw.getSeps()
        rc = RECONSTRUCT()
        rc.fill_leaves(these_leaves.values())
        rc.gen_layers()
        rc.calc_prbs()
        rc.reconstruct(0)
        #cProfile.runctx('rc.reconstruct(0)',globals(),locals())
        '''
        print "THIS WAS THE ORIGINAL TREE:"
        print orig_tree
        print "THIS IS OUR PREDICTION:" 
        print rc.PRED_TREE
        '''
        if len(rc.PRED_TREE) > 0:
            for i in orig_tree:
                if len(orig_tree[i]) == 0:
                    orig_tree[i] = "XXX"
            ev = evaluate.Evaluate()
            ev_res =  ev.run_evaluate(orig_tree, rc.PRED_TREE)
            rc.logger.warning("This is the evaulation result: %s" , str(ev_res))
            #print k, str(ev_res)
            
            evalss = []
            for f in list(ev_res)[0]:
                evalss.append(sum(f))
                
            if min(evalss) == 0:
                this_succ +=1
            else:
                this_with_error +=1
        else:
            this_not_predicted+=1
        #print k, "*******************************************"
    succ.append(this_succ)
    with_error.append(this_with_error)
    not_predicted.append(this_not_predicted)
    print imp_sampling, this_succ, this_with_error, this_not_predicted

#visualizetree.run_all(orig_tree, rc.PRED_TREE, 'orig', 'pred')
print range(5,15)
print succ
print with_error 
print not_predicted