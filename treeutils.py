import const
import sequtils
from collections import defaultdict
import math

# I need to use some ID for each sequence
# Using dict of dicts
# TODO:
# Lots of duplications here. Lets see what I can do later
def encode_to_dict(init_layers):
    dict_layers = defaultdict(dict)
    roots = init_layers[0]
    node_num = 1
    # FIXME:
    #ADD separate roots        
    for node in roots:
        if sequtils.compare_seqs(node, const.ROOT[0]):
            dict_layers[0][node_num] = node
            node_num += 1     
            
    for layer in range(0,const.LEVEL+1):
        this_layer = init_layers[layer]
        node_num = 1
        for node in this_layer:
            dict_layers[layer][node_num] = node
            node_num += 1
            
    return dict_layers
    

# Something Stupid for getting the generation number
def get_layer_fromtreeid(treeid):
    return math.floor(math.log(treeid + 1, 2))
    
    
# Just generating barcodes for nodes to distinguish them easier
def gen_postx(t_size):
    post_dict = {}
    post_dict[0] = "p"
    for m in range(1,t_size):
        if m %2 ==0:
            parent = (m-2)/2
            postx = post_dict[parent]+"1"
        else:
            parent = (m-1)/2
            postx = post_dict[parent]+"0"
        post_dict[m] = postx
    return post_dict
    
    
# Collects info on common parents
# What nodes have specific nodes to pair with
# Helps with not checking uniqely paired nodes
def extract_pairs_info(self, info_mat):
    counter = {}
    pairs = defaultdict(list)
    rng = range(len(info_mat))
    for rowind in rng:
        for colind in rng:
            if len(info_mat[rowind][colind]) > 0 :
                if rowind+1 in counter:
                    counter[rowind+1] = counter[rowind+1] +1
                    pairs[rowind+1].append(colind+1)
                else:
                    counter[rowind+1] = 1
                    pairs[rowind+1].append(colind+1)
    return counter, pairs   