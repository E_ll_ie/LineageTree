from difflib import SequenceMatcher
import needle
from collections import defaultdict
import treeutils
import const
import random
import math
import evaluate
import forward
import probcalculator 
import utils
import parents
import sequtils

class GREEDYJOIN():
    def __init__(self):
        self.leaves = []
        self.prefix_table = defaultdict(list)
        self.children = defaultdict(list)
        self.prefixes = defaultdict(list)
        self.tree = [[]]* (2*(2**const.LEVEL)-1)
        self.t_size = 2*(2**const.LEVEL)-1
        self.post_dict = treeutils.gen_postx(self.t_size)
        self.tree1 = {}
        self.tree2 = {}

    ## Compares two trees two see if they're equal
    def tree_eq(self,ind1,ind2):
        if len(ind1) > (const.LEVEL+1 ) or len(ind2) > (const.LEVEL+1):
            return True
        if self.tree1[ind1] != self.tree2[ind2]:
            return False
        if self.tree_eq(ind1+"0",ind2+"0") and self.tree_eq(ind1+"1", ind2+"1"):
            return True
        if self.tree_eq(ind1+"0",ind2+"1") and self.tree_eq(ind1+"1", ind2+"0"):
            return True
        return False    

    
    def add_children_indexes(self, childlist, index, levs):
        if levs > 0:
            childlist.append(2*index+1)
            childlist = self.add_children_indexes(childlist, 2*index+1, levs-1)
            childlist.append(2*index+2)
            childlist = self.add_children_indexes(childlist, 2*index+2, levs-1)
        
        return childlist
        

    # Calculate the probability of observing this tree after it is made      
    def calculate_tree_probs(self, tree):
        probs  = 0
        blacklist = []
        rootx = tree[0].count("X")
        if rootx > 0:
            probs = float("-inf")
            return probs
        if len(tree[0]) == 0:
            startfrom = 1
        else:
            startfrom = 0
        for ind in range(startfrom,2**const.LEVEL - 1):
            if ind not in blacklist :
                leftx = tree[2*ind+1].count("X") 
                rightx = tree[2*ind+2].count("X")
                if leftx == 0 :               
                    probs+= probcalculator.calc_prbXY(tree[ind], tree[2*ind+1])
                    '''
                    if probcalculator.calc_prbXY(tree[ind], tree[2*ind+1]) == float("-inf"):
                        print "============"
                        print tree[ind]
                        print tree[2*ind+1]
                        print tree[2*ind+2]
                        print "============"
                        break
                    '''
                elif leftx > 0 :
                    probs+= self.find_prob_more_gen(tree[ind], tree[2*ind+1][leftx:], leftx+1)
                    blacklist.append(2*ind+1)      
                    blacklist+= self.add_children_indexes([],2*ind+1, leftx-1)
                    
                if rightx == 0:
                    probs+= probcalculator.calc_prbXY(tree[ind], tree[2*ind+2])
                elif rightx > 0 :
                    probs+= self.find_prob_more_gen(tree[ind], tree[2*ind+2][rightx:], rightx+1)
                    blacklist.append(2*ind+2)                    
                    blacklist+= self.add_children_indexes([],2*ind+2, rightx -1)
                    
        return probs


    def improve_tree_ifpossible(self, tree):
        probs  = 0
        blacklist = []
        startfrom = 0
        rootx = tree[0].count("X")
        if rootx > 0:
            probs = float("-inf")
            return probs
        for ind in range(startfrom,2**const.LEVEL - 1):
            if ind not in blacklist :
                leftx = tree[2*ind+1].count("X") 
                rightx = tree[2*ind+2].count("X")
                if leftx == 0 :
                    this_prob = probcalculator.calc_prbXY(tree[ind], tree[2*ind+1])
                    if this_prob == float("-inf") and  len(tree[ind]) < len(tree[2*ind+1]):
                        lengdif = len(tree[2*ind+1]) - len(tree[ind])
                        if tree[ind][:-3] == tree[2*ind+1][:-3-lengdif]:
                            tree[2*ind+1] = tree[2*ind+1][:-3-(lengdif-max(const.INS_LEN_PRB.keys()))] + tree[2*ind+1][-3:]
                        elif tree[ind][:-4] == tree[2*ind+1][:-4-lengdif]:
                            tree[2*ind+1] = tree[ind][:-4-(lengdif-max(const.INS_LEN_PRB.keys()))] + tree[ind][-4:]
                        this_prob = probcalculator.calc_prbXY(tree[ind], tree[2*ind+1])
                    probs+= this_prob
                    
                elif leftx > 0 :
                    probs+= self.find_prob_more_gen(tree[ind], tree[2*ind+1][leftx:], leftx+1)
                    blacklist.append(2*ind+1)      
                    blacklist+= self.add_children_indexes([],2*ind+1, leftx-1)
                    
                if rightx == 0:
                    this_prob = probcalculator.calc_prbXY(tree[ind], tree[2*ind+2])
                    if this_prob == float("-inf") and len(tree[ind]) < len(tree[2*ind+2]):
                        lengdif = len(tree[2*ind+2]) - len(tree[ind])
                        if tree[ind][:-3] == tree[2*ind+2][:-3-lengdif]:
                            tree[2*ind+2] = tree[2*ind+2][:-3-(lengdif-max(const.INS_LEN_PRB.keys()))] + tree[2*ind+2][-3:]
                        elif tree[ind][:-4] == tree[2*ind+2][:-4-lengdif]:
                            tree[2*ind+2] = tree[2*ind+2][:-4-(lengdif-max(const.INS_LEN_PRB.keys()))] + tree[2*ind+2][-4:]
                        this_prob = probcalculator.calc_prbXY(tree[ind], tree[2*ind+2])
                    probs+= this_prob
                elif rightx > 0 :
                    probs+= self.find_prob_more_gen(tree[ind], tree[2*ind+2][rightx:], rightx+1)
                    blacklist.append(2*ind+2)                    
                    blacklist+= self.add_children_indexes([],2*ind+2, rightx -1)      
                    
        return probs
        
    ## When a leaf is missing, I usually fill the parent with the other available leaf.
    ## Then the grandparent is generated lookiong at this leaf, which is two generations further, and not one
    ## Hence a problem comes up when calculating the most likely tree.
    ## Here I want to find the parent, using the grandparent and the grandchild.
    ## Or I want to calculate the probability of going from grandparent to child in 2 generations
    ## The thing is I want to start with just considering insertions, which is more in line with our
    ## greedy assumption here.
    ## I'll have another one with all different parents generated for the grandchild later.
    def find_middle_node(self, this_grandparent, this_grandchild):
        problist = []
        insertion_length = len(this_grandchild) - len(this_grandparent)
        insertion = this_grandchild[-3-insertion_length:-3]
        prefix = this_grandchild[:-3-insertion_length]
        for first_insertion_length in const.INS_LEN_PRB:
            second_insertion_length = insertion_length - first_insertion_length
            if second_insertion_length in const.INS_LEN_PRB:
                this_possible_parent = prefix + insertion[:first_insertion_length]
                problist.append(probcalculator.calc_prbXY(this_grandparent, this_possible_parent) + probcalculator.calc_prbXY(this_possible_parent, this_grandchild)) 
        if len(problist) > 0 :
            prob = utils.logSumExp(problist) 
        else:
            prob = float('-Inf')
        return prob
    
    
    ## make the last function a little more general:
    def find_prob_more_gen(self, this_grandparent, this_grandchild, dist):
        prob_list = []      
        this_level = [{} for j in range(dist+1)]
        this_level[0]["1"] = (this_grandchild,1)
        for i in range(1,dist+1):
            for temp_node_id in this_level[i-1]:
                temp_parents = [j for j in parents.gen_parents(this_level[i-1][temp_node_id][0])]
                temp_parents.sort(key=lambda x: x[1], reverse= True)
                for counter in range(len(temp_parents)):
                    #print this_level
                    new_id = temp_node_id+"_"+str(counter+1)
                    this_level[i][new_id] = temp_parents[counter]
                    
        for this_id in this_level[-1]:
            if sequtils.compare_seqs(this_level[-1][this_id][0],this_grandparent):
                prob = 0
                ids = this_id.split("_")
                for m in range(2,len(ids)):
                    prob += this_level[m-1]["_".join(ids[:m])][1]
                prob_list.append(prob)
        if len(prob_list) > 0 :
            prob = utils.logSumExp(prob_list) 
        else:
            #print parent + "==="+ child
            prob = float('-Inf')
        return prob
                
                    
                    

    ## You need to clean this. seriously.    
    def find_possible_parent(self, seq1, seq2):
        xflag = False
        permitted_insertion_length = max(const.INS_LEN_PRB.keys())
        if len(seq1) > 0 and len(seq2) > 0 :
            if seq1[0]!="X" and seq2[0]!="X":
                #FIXME 
                ##DO NOT FORGET --- CHANGE BACK --- NO plus one needed
                permitted_insertion_length = max(const.INS_LEN_PRB.keys())+1
            else:
                #print "***********"
                #print seq1,seq2
                permitted_insertion_length = (max(seq1.count("X"),seq2.count("X"))+1)*max(const.INS_LEN_PRB.keys())
                xnums = max(seq1.count("X"),seq2.count("X"))+1
                seq1 = seq1[seq1.count("X"):]
                seq2 = seq2[seq2.count("X"):]
                xflag = True
                
                #print seq1,seq2,xnums
                #print "***********"
        s = SequenceMatcher()
        #if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
        #    s.set_seq1(seq1[:-4])
        #    s.set_seq2(seq2[:-4])
        #    matches = s.get_matching_blocks()
        if seq1!=seq2 and seq1[-3:] == seq2[-3:]:
            s.set_seq1(seq1[:-3])
            s.set_seq2(seq2[:-3])
            matches = s.get_matching_blocks()
        else:
            s.set_seq1(seq1)
            s.set_seq2(seq2)
            matches = s.get_matching_blocks()
            
        #if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
        #    parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:] 

        if seq1!=seq2 and seq1[-3:] == seq2[-3:]:
            parent = seq1[matches[0][0]:matches[0][2]] + seq1[-3:]
            
        elif len(matches) >= 3 and len(seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]) in [3,4]: 
            parent = seq1[matches[0][0]:matches[0][2]]+seq1[matches[-2][0]:matches[-2][0]+matches[-2][2]]
            #print seq1,seq2, parent
        elif seq1 == seq2 :
            parent = seq1
        elif seq1.endswith("TGA") or seq2.endswith("TGA"):
            parent = seq1[matches[0][0]:matches[0][2]] + "TGA"
        else:
            parent = seq1[matches[0][0]:matches[0][2]] 
        
        possible_parent = ""
        if abs(len(parent)-len(seq1)) <= permitted_insertion_length and  abs(len(parent)-len(seq2)) <= permitted_insertion_length :
            # FIXME 
            #if xflag:
                #parent = xnums*"X"+ parent
            if len(parent) > len(possible_parent):
                possible_parent =   parent
            # TEMPORARY
            #return parent
        else:
            if seq1!=seq2 and seq1[-4:] == seq2[-4:]:
                s.set_seq1(seq1[:-4])
                s.set_seq2(seq2[:-4])
                matches = s.get_matching_blocks()
                parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:]
                if abs(len(parent)-len(seq1)) <= permitted_insertion_length and  abs(len(parent)-len(seq2)) <= permitted_insertion_length :
                    # FIXME 
                    #if xflag:
                    #        parent = xnums*"X"+ parent
                    if len(parent) > len(possible_parent):
                        possible_parent = parent
                    # TEMPORARY
                    #return parent
            elif seq1!=seq2 and seq1[-3:] == seq2[-3:]:                
                s.set_seq1(seq1[:-3])
                s.set_seq2(seq2[:-3])
                matches = s.get_matching_blocks()
                check_1 = seq1[-4]
                check_2 = seq2[matches[0][2]+1]
                
                check_3 = seq2[-4]
                check_4 = seq1[matches[0][2]+1]
                
                if check_1==check_2:
                    parent = seq1[matches[0][0]:matches[0][2]] + seq1[-4:]
                elif check_3 == check_4:
                    parent = seq1[matches[0][0]:matches[0][2]] + seq2[-4:]
                if abs(len(parent)-len(seq1)) <= permitted_insertion_length and  abs(len(parent)-len(seq2)) <= permitted_insertion_length :
                    # FIXME 
                    #if xflag:
                            #parent = xnums*"X"+ parent
                    if len(parent) > len(possible_parent):
                        possible_parent = parent                    
                    # TEMPORARY
                    #return parent
            
        #print possible_parent
        return possible_parent
            
    def find_common_blocks(self, seq1, seq2):
        blocks = needle.needle(seq1, seq2).split(" ")
        return blocks
        
        
    def pair_seqs(self, this_level_num):
        these_prefixes = self.prefixes[this_level_num]
        rng = range(len(these_prefixes))
        self.prefix_table[this_level_num] = [[[] for i in rng] for j in rng]
        for i in range(len(these_prefixes)):
            for j in range(i,len(these_prefixes)):
                if i == j :
                    self.prefix_table[this_level_num][i][j]= ""
                elif these_prefixes[i]=="XXX" and these_prefixes[j]!="XXX":
                    self.prefix_table[this_level_num][i][j] = "X"+these_prefixes[j]
                    self.prefix_table[this_level_num][j][i] = "X"+these_prefixes[j]
                elif these_prefixes[i]!="XXX" and these_prefixes[j]=="XXX":
                    self.prefix_table[this_level_num][i][j] = "X"+these_prefixes[i]
                    self.prefix_table[this_level_num][j][i] = "X"+ these_prefixes[i]
                elif these_prefixes[i] == "XXX" and these_prefixes[j] == "XXX":
                    self.prefix_table[this_level_num][i][j] = "XXX"
                    self.prefix_table[this_level_num][j][i] = "XXX"
                else:
                    self.prefix_table[this_level_num][i][j] = self.find_possible_parent(these_prefixes[i], these_prefixes[j])
                    self.prefix_table[this_level_num][j][i] = self.find_possible_parent(these_prefixes[i], these_prefixes[j])
                
    ## Pair the nodes in a greedy manner
    def greedy_pairs(self,this_level_num):
        max_len = -1
        pairs = []
        indexes = range(2**this_level_num)
        ending = (len(indexes)/2) 
        while len(pairs) < ending :
            max_len = -1
            for i in range(len(indexes)-1):
                for j in range(i+1, len(indexes)):                    
                    if len(self.prefix_table[this_level_num][indexes[i]][indexes[j]]) >= max_len:
                        max_len = len(self.prefix_table[this_level_num][indexes[i]][indexes[j]])                 
                        max_pair = [indexes[i],indexes[j]]
            to_be_removed1 = max_pair[0]
            to_be_removed2 = max_pair[1]
            indexes.remove(to_be_removed1)
            indexes.remove(to_be_removed2)
            pairs.append(max_pair)
        for p in pairs:
            self.prefixes[this_level_num-1].append(self.prefix_table[this_level_num][p[0]][p[1]])
            self.children[this_level_num-1].append((p[0],p[1]))
        


    def greedy_pairs_all(self):
        for this_level_num in range(const.LEVEL, 0, -1):
            self.pair_seqs(this_level_num)
            self.greedy_pairs(this_level_num)
        self.feel_tree()
        
        
    def fill_children(self, k, m):
        child_level = treeutils.get_layer_fromtreeid(k)
        if child_level < const.LEVEL:
            these_children = self.children[child_level][m]
            self.tree[2*k+1] = (self.prefixes[child_level+1][these_children[0]],these_children[0])
            self.tree[2*k+2] = (self.prefixes[child_level+1][these_children[1]],these_children[1])   
            self.fill_children(2*k+1, these_children[0])
            self.fill_children(2*k+2, these_children[1])           
        
        
    def feel_tree(self):        
        self.tree[0] = (self.prefixes[0][0],0)
        self.fill_children(0,0)
            
    
    def njprocess(self, leaves):
        self.prefixes[const.LEVEL] = leaves
        self.greedy_pairs_all()

    ## main function here, it calls everything else
    def rungreedyreconstruct(self, leaves, rounds):
        possible_leaves = []
        for perm in range(rounds):
            leaves_ids = range(len(leaves))
            l = list(zip(leaves, leaves_ids))
            random.shuffle(l)
            shuffled_leaves, shuffled_ids = zip(*l)

            iddict = {}
            for i in range(len(leaves)):
                iddict[i] = shuffled_ids[i]
            self.njprocess(shuffled_leaves)

            these_possible_tree = []
            for node in self.tree[:-len(leaves)]:
                ## FIXME
                ##these_possible_tree.append(node[0][node[0].count("X"):])
                these_possible_tree.append(node[0])
            for node in self.tree[len(self.tree)-len(leaves):]:
                these_possible_tree.append(node[0])
                
            pred_tree = {}
            for i in range(len(self.tree)):
                pred_tree[self.post_dict[i]] = these_possible_tree[i]
            possible_leaves.append(pred_tree)
            
            
            self.leaves = []
            self.prefix_table = defaultdict(list)
            self.children = defaultdict(list)
            self.prefixes = defaultdict(list)
            self.tree = [[]]* (2*(2**const.LEVEL)-1)  
        return possible_leaves
     
     
     
import visualizetree

tracks = []
scores = []
with_error = []
not_predicted = []
succ = []
upper_bounds =range(30,75,5) 
for upper_bound in upper_bounds:
    this_with_error = 0
    this_not_predicted = 0
    this_succ = 0
    for k in range(50):    

        fw = forward.FORWARD()
        orig_tree = fw.genTree()
        interval_nodes, these_leaves = fw.getSeps()
        
        
        gj = GREEDYJOIN()
        preds = gj.rungreedyreconstruct(these_leaves.values(),upper_bound)        
        ev = evaluate.Evaluate()
        
        likelihood = float("-inf")
        max_tree = {}
        max_trees = []
        probs = []
        for t in preds:
            prob = gj.improve_tree_ifpossible(visualizetree.list_from_dict(t,gj.post_dict))
            probs.append(prob)
            if prob > likelihood:
                max_tree = t
                likelihood = prob
        #print likelihood
        for this_tree_ind in range(len(preds)):
            if probs[this_tree_ind] == likelihood:
                max_trees.append(preds[this_tree_ind])
        
            
        flag = False
        for t in max_trees:
        #if True:
            max_tree = t
            if len(max_tree) > 0 and likelihood!=float("-inf"):
                flag = True
                ev_res = ev.run_evaluate(orig_tree, max_tree)   
                evalss = []
                for f in list(ev_res)[0]:
                    evalss.append(sum(f))
                temp_ev_val = min(evalss)
                #print k, ev.run_evaluate(orig_tree, max_tree) 
                if temp_ev_val == 0:
                    this_succ+=1
                else:
                    this_with_error+=1
                break
        if not flag:
            this_not_predicted+=1
                #print k, "SORRY GUYS!"
        #print "*******************************************"
    succ.append(this_succ)
    not_predicted.append(this_not_predicted)
    with_error.append(this_with_error)
    print upper_bound, this_succ, this_with_error, this_not_predicted
print upper_bounds
print succ
print with_error
print not_predicted