import sequtils
import const
import math
import utils
import numpy as np
# Calculate probability of having this child for this parent                     
def calc_prbXY(parent, child):
    prob = 0      
    problist = []
    
    ##Adding this part for missing nodes
    ##First Case: Cannot have ampty parent, and not empty children

    if len(parent) == 0 and len(child) > 0 :
        return float('-Inf')
    #TODO:
    #Not sure about this   
    ##Second Case: Parent is not empty, but children is
    ##This can cause problem actually, because the child can be anything
    if len(parent) >= 0 and len(child) == 0:
        return 0
    
    # Case of Deletion
    if len(parent) > len(child):
        len_diff = len(parent) - len(child)
        if len_diff in const.DEL_LEN_PRB.keys():
            for loc in const.DEL_POS_PRB.keys():
                for del_dir in const.DEL_DIR_PRB.keys():
                    nuc_probs = 1
                    if del_dir == "R":
                        temp = parent[0:loc] + parent[loc+len_diff:]
                        deleted = parent[loc : loc+len_diff]
                    else:
                        temp = parent[0:loc-len_diff] + parent[loc]
                        deleted = parent[loc-len_diff:loc]
                    flagged = True
                    flagged = sequtils.compare_seqs(temp, child)
                    if flagged:
                        for let in deleted:
                            nuc_probs= nuc_probs*const.NUCL_PRO_DEL_DICT[let]
                            #nuc_probs= 1
                        # raw probabilities
                        # come back and 
                        # FIXME
                        ##prob = prob + (const.DEL_PROB) * const.DEL_LEN_PRB[len_diff] * const.DEL_POS_PRB[loc] *nuc_probs
                        # Go to log domain
                        problist += [math.log(const.DEL_PROB) + math.log(const.DEL_LEN_PRB[len_diff]) + math.log(const.DEL_POS_PRB[loc]) + math.log(nuc_probs)+ math.log(const.DEL_DIR_PRB[del_dir])]
                    
    # Case of Insertion
    elif len(parent) < len(child):
        len_diff = len(child) - len(parent)
        if len_diff in const.INS_LEN_PRB.keys():
            for loc in const.INS_POS_PRB.keys():
                nuc_probs = 1
                temp = child[0:loc-len_diff] + child[loc:]
                
                flagged = sequtils.compare_seqs(temp, parent)
                #print parent, child, temp, flagged
                if flagged:
                    inserted = child[loc:loc+len_diff]
                    for let in inserted:
                        nuc_probs= nuc_probs*const.NUCL_PRO_INS_DICT[let]  
                        #nuc_probs= 1
                    # raw probabilities
                    # come back and 
                    # FIXME                                     
                    ##prob = prob + const.INS_PROB * const.INS_LEN_PRB[len_diff] * const.INS_POS_PRB[loc] * nuc_probs
                    # Go to log domain
                    problist += [math.log(const.INS_PROB) + math.log(const.INS_LEN_PRB[len_diff]) + math.log(const.INS_POS_PRB[loc]) + math.log(nuc_probs)]
                    
    elif parent == child :
        temp = child
        flagged = sequtils.compare_seqs(temp, parent)
        if flagged:
            # raw probabilities
            # come back and 
            # FIXME   
            ##prob = prob + const.NO_PROB
            problist += [math.log(const.NO_PROB)]
    if len(problist) > 0 :
        prob = utils.logSumExp(problist) 
        #print problist
    else:
        #print parent + "==="+ child
        prob = float('-Inf')
    if np.isnan(prob):
        print problist
    return prob
    
    
#print calc_prbXY("GGCCCAGACTGAGCACTGA","GGCCCAGACTGAGCACGAGTGA")