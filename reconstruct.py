import const
from collections import defaultdict
import math
import forward
import numpy as np 
import random
import Nutils
import treeutils
import sequtils
import probcalculator as prbc
import parents
import utils
import sys
import scores
import evaluate
'''
This function is supposed to get two sequence
Find all possible merging cases and their probabilities.
'''  
#random.seed(8)
#np.random.seed(8)
class RECONSTRUCT:
    def __init__(self):
        self.init_layers = defaultdict(list)
        self.COMMON_PARENTS_DICT = defaultdict(list)
        self.leaves = []
        self.dict_layers = defaultdict(dict)
        self.prbs_tables =  defaultdict(dict)
        self.t_size = 2*(2**const.LEVEL)-1
        self.tree_locs_probs = defaultdict(dict)
        self.post_dict = {}
        self.PRED_TREE = {}
        self.init_parent_dict = defaultdict(dict)
        self.possible_leaves = []
        
        
    # Loading the leaves from a list
    def fill_leaves(self, given_leaves):
        self.leaves = given_leaves
        self.init_layers[const.LEVEL] = given_leaves
        


    def recursive(self, a, b):
        n = len(a)
        if n == 0 or n == 1:
            bt = [j for i in b for j in i]
            self.possible_leaves.append(bt)
            return
        for i in range(1,n):
            this_one = [item for item in a if item not in [a[0],a[i]]]
            that_one  = b + [[a[0],a[i]]]
            #print a[i],a[0]
            if len(self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]) > 0 :
                self.recursive(this_one, that_one)
            
            
    def recursive_withprobs(self, a, b , prob):
        n = len(a)
        if n == 0 or n == 1:
            bt = [j for i in b for j in i]
            self.possible_leaves.append((bt,prob))
            return
        for i in range(1,n):
            this_one = [item for item in a if item not in [a[0],a[i]]]
            that_one  = b + [[a[0],a[i]]]
            
            #print a[i],a[0]
            if len(self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]) > 0 :
                self.recursive_withprobs(this_one, that_one, prob)
                prob += utils.logSumExp([t[1] for t in self.COMMON_PARENTS_DICT[const.LEVEL][a[0]][a[i]]])
    ############################################################################################################################    
    # Loops over the generation, starting from the leaves
    # keeps all possible cases
    def gen_layers(self):
        for layer in range(const.LEVEL, 0, -1):
            #print layer
            self.gen_pre_layer(self.init_layers[layer], layer)   
            
        self.dict_layers = treeutils.encode_to_dict(self.init_layers)
        self.post_dict = treeutils.gen_postx(self.t_size)

        
    # Obviously generate possible seqs for previous layer
    def gen_pre_layer(self,this_layer, this_layer_num):
        init_pre_layer = set()
        node_num = 0
        for this_node in this_layer:
            generated_parents = parents.gen_parents(this_node)
            self.init_parent_dict[this_layer_num][node_num] = generated_parents
            init_pre_layer.update(generated_parents)
            node_num+=1
        self.gen_pre_layer_commons(self.init_parent_dict[this_layer_num], this_layer_num)
        #updated_pre_layer = init_pre_layer
        #self.init_layers[this_layer_num-1]= updated_pre_layer
        #eliminations, additions  = Nutils.readjust_deletions(updated_pre_layer, this_layer)
        #updated_pre_layer.update(additions)
        
        #self.init_layers[this_layer_num-1]= list(updated_pre_layer.difference(set(eliminations)))
        
        
    # For parents generated in each layer, 
    # Generate another list of common parents
    # So we can keep track of those parents that can really parent two children
    def gen_pre_layer_commons(self, init_prev_layer, this_layer_num):
        nodes = init_prev_layer 
        # For now only for the leaves I keep all the probabilities
        if this_layer_num == const.LEVEL :
            self.COMMON_PARENTS_DICT[this_layer_num] = [[[] for i in range(2**this_layer_num)] for j in range(2**this_layer_num)]
        #print nodes        
        for this_ind in range(len(nodes)):
            for that_ind in range(this_ind+1, len(nodes)):                                
                ## TODO Define this dictionary again I guess
                ## With id, layer and sequence
                this_common_parents = []
                this_common_parents = parents.compare_parents_withPRB(nodes[this_ind],nodes[that_ind])
                #this_common_parents = parents.sumprobs_common_parents(parents.compare_parents_withPRB(nodes[this_ind],nodes[that_ind]))
                temp = set()
                for p in this_common_parents:
                    temp.add(p[0])
                self.init_layers[this_layer_num - 1] += temp 
                if this_layer_num == const.LEVEL:                                                          
                    self.COMMON_PARENTS_DICT[this_layer_num][this_ind][that_ind]  = list(set(this_common_parents))            
                    self.COMMON_PARENTS_DICT[this_layer_num][that_ind][this_ind]  = list(set(this_common_parents))  
        
        #updated_pre_layer = set(self.init_layers[this_layer_num - 1])
        #eliminations, additions  = Nutils.readjust_deletions(updated_pre_layer, set(self.init_layers[this_layer_num]))
        #updated_pre_layer.update(additions)
        #self.init_layers[this_layer_num-1]= list(updated_pre_layer.difference(set(eliminations)))
        self.init_layers[this_layer_num - 1] =  list(set(self.init_layers[this_layer_num - 1]))
        if this_layer_num == const.LEVEL:
            if const.LEAVES_CHOICE == "ALL":
                self.recursive_withprobs(range(len(nodes)), [], 0)
                print len(self.possible_leaves)
            elif const.LEAVES_CHOICE == "NJ1":
                sc = scores.SCORE()
                sc.info_mat = self.COMMON_PARENTS_DICT[const.LEVEL]
                self.possible_leaves =  sc.run_njbalanced_prob(range(len(self.leaves)),const.UPPER_BOUND)
                
            elif const.LEAVES_CHOICE == "NJ2" :
                sc = scores.SCORE()
                self.possible_leaves = sc.run_njbalanced_dist(self.init_layers[this_layer_num],const.UPPER_BOUND)
                
        print str(const.LEVEL - this_layer_num) + "  of layers, have a set of possible sequences right now"
     ############################################################################################################################   

    # The fun part of course.
    # After having a set for each layer
    # For each layer and the next one calculates all probabilities        
    def calc_prbs(self):
        for layer in range(0, const.LEVEL):
            for parents_id in self.dict_layers[layer]:
                for child_id in self.dict_layers[layer+1]:
                    self.prbs_tables[layer][(parents_id, child_id)] = \
                    prbc.calc_prbXY(self.dict_layers[layer][parents_id],self.dict_layers[layer+1][child_id] )
                    #if np.isnan(self.prbs_tables[layer][(parents_id, child_id)]):
                    #    print layer, parents_id, child_id,self.prbs_tables[layer][(parents_id, child_id)]
            
    
        
    # Calculate the probability of seeing each seq in each position of tree
    # In a recursive manner
    def Prob(self, pos_in_tree, start_node):
        layer = treeutils.get_layer_fromtreeid(pos_in_tree)
        #print start_node, layer
        if layer == const.LEVEL:
            return 0
        else:
            probl = 0
            problist = []
            if layer+1 == const.LEVEL:
                # Don't forget to set the id in the leaves level as this
                layer_to_look = [self.leaves[2*pos_in_tree+1 - 2**(const.LEVEL) + 1]+1] 
                layer_to_look_other = [self.leaves[2*pos_in_tree+2 - 2**(const.LEVEL) + 1]+1] 
            else: 
                layer_to_look = self.dict_layers[layer+1] 
                layer_to_look_other = self.dict_layers[layer+1]
            for child1_id in layer_to_look:                        
                for child2_id in layer_to_look_other:
                    if (layer+1, child1_id) in self.tree_locs_probs[2*pos_in_tree+1]:
                        child1_prob = self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)]
                        #if np.isnan(child1_prob):
                        #    print self.tree_locs_probs[2*pos_in_tree+1]
                    else:
                        self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)] = self.Prob(2*pos_in_tree+1,child1_id)
                        child1_prob = self.tree_locs_probs[2*pos_in_tree+1][(layer+1, child1_id)]
                        #if np.isnan(child1_prob):
                        #    print child1_id, 2*pos_in_tree+1
                        
                    if (layer+1, child2_id) in self.tree_locs_probs[2*pos_in_tree+2]:
                        child2_prob = self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)]
                    else:
                        self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)] = self.Prob(2*pos_in_tree+2,child2_id)
                        child2_prob = self.tree_locs_probs[2*pos_in_tree+2][(layer+1, child2_id)]                        
                    # Raw probabilities
                    # FIXME keep this for now
                    # probl+=child1_prob* self.prbs_tables[layer][(start_node, child1_id)] * child2_prob * self.prbs_tables[layer][(start_node, child2_id)]
                    
                    #Lets move to log domain 
                    #print layer, child1_id, child2_id, child2_prob, child1_prob , self.prbs_tables[layer][(start_node, child1_id)], self.prbs_tables[layer][(start_node, child2_id)]
                    problist += [child1_prob + self.prbs_tables[layer][(start_node, child1_id)] + child2_prob + self.prbs_tables[layer][(start_node, child2_id)]]
            #print problist
            
            probl = utils.logSumExp(problist)
            #if np.isnan(probl):
                #print problist, pos_in_tree
                #sys.exit("Error message")

            #print probl
            return probl
        
        
        
    # Calculate the probabilities of seeing each sequence in each    
    def reconstruct(self):
        possible_trees = []
        probs = []
        possible_roots = []
        self.possible_leaves.sort(key=lambda x: x[1], reverse=True)
        # For each generated root like sequence, we calculate the probabilities
        print "There are " + str(len(self.possible_leaves)) + " permutations to check"
        for r_id in self.dict_layers[0]:
            this_r= self.dict_layers[0][r_id]
            if sequtils.compare_seqs(this_r, const.ROOT[0]):
                possible_roots.append(r_id)
        print "There are " + str(len(possible_roots)) + " roots to check"
        root_counter = 0

        for possible_root_id in possible_roots:
            possible_root = self.dict_layers[0][possible_root_id]
            print possible_root
            # Here: Probs for this root
            root_counter +=1
            leaves_counter = 0
            for this_leaves_perms_tuple in self.possible_leaves[:const.UPPER_BOUND]:
                this_leaves_perms = this_leaves_perms_tuple[0]
                tree = [""] * self.t_size
                nodeids = [[]]  * self.t_size
                self.leaves = this_leaves_perms
                leaves_counter+=1
                this_prob = self.Prob(0, possible_root_id)
                #print this_prob
                if this_prob > float('-inf'):
                    probs.append(this_prob)
                    #print this_prob
                    tree[0] = possible_root
                    nodeids[0] = possible_root_id
                    for child_tpos in range(1, self.t_size):
                        parent_tpos = int(math.floor(float(child_tpos-0.001)/2))
                        this_parent_id = nodeids[parent_tpos]
                        this_layer = treeutils.get_layer_fromtreeid(child_tpos)
                        this_p = float('-inf')
                        for possible_child_id in self.dict_layers[this_layer]:
                            # FIXME you shouldn't need this
                            if (this_layer, possible_child_id) in self.tree_locs_probs[child_tpos]:
                                this_child = self.dict_layers[this_layer][possible_child_id]   
                                #raw probability values
                                #FIXME
                                '''
                                if self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] * \
                                    self.prbs_tables[this_layer -1 ][(this_parent_id, possible_child_id)] > this_p:
                                    this_p = self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] * \
                                        self.prbs_tables[this_layer - 1][(this_parent_id, possible_child_id)]
                                '''
                                # Lets go to log domain
                                #print child_tpos, parent_tpos, this_parent_id, self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)]
                                if self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                    self.prbs_tables[this_layer -1 ][(this_parent_id, possible_child_id)] > this_p:
                                    this_p = self.tree_locs_probs[child_tpos][(this_layer, possible_child_id)] + \
                                        self.prbs_tables[this_layer - 1][(this_parent_id, possible_child_id)]
                                    tree[child_tpos] = this_child
                                    nodeids[child_tpos] = possible_child_id


                    possible_trees.append(tree)
                    #print this_prob, tree
                if leaves_counter % 20 == 0:
                    print "From Root" + str(root_counter) + " ;" + str(leaves_counter) + "  is done!"
                self.tree_locs_probs = defaultdict(dict)
        print max(probs)
        #print sorted(probs)
        pred_tree = possible_trees[probs.index(max(probs))]
        #print "\n".join(map(str,pred_tree))
        #print probs
        #print possible_trees
        for i in range(len(pred_tree)):
            self.PRED_TREE[self.post_dict[i]] = pred_tree[i]
                        
        
        
'''    
fw = forward.FORWARD()
rc = RECONSTRUCT()
orig_tree = fw.genTree()
interval_nodes, these_leaves = fw.getSeps()
'''

#orig_tree = {'P000':'GGCCCAGACTGAGCACGGAAATGA', 'P001':'GGCCCAGACTGAGCACGGAACGGTGA', 'P010':'GGCCCAGACTGAGCACGGAATAGCGTGTGA', 'P011':'GGCCCAGACTGAGCACGGAATAGTTGA', 'P100':'GGCCCAGACTGAGCACGAGGTGA', 'P101':'GGCCCAGACTGAGCACGTGA', 'P110':'GGCCCAGACTGAGCACGAATCAGGTGA', 'P111':'GGCCCAGACTGAGCACGAATGCTCTGA', 'P00':'GGCCCAGACTGAGCACGGAATGA', 'P01':'GGCCCAGACTGAGCACGGAATAGTGA', 'P10':'GGCCCAGACTGAGCACGTGA', 'P11':'GGCCCAGACTGAGCACGAATGTGA', 'P1':'GGCCCAGACTGAGCACGTGA', 'P0':'GGCCCAGACTGAGCACGGAATGA', 'P':'GGCCCAGACTGAGCACGTGA','P0000':'GGCCCAGACTGAGCACGGAAATGA', 'P0001':'GGCCCAGACTGAGCACGGAAATGA', 'P0010':'GGCCCAGACTGAGCACGGAACGGTGGTGA', 'P0011':'GGCCCAGACTGAGCACGGAACGGGGATGA', 'P0100':'GGCCCAGACTGAGCACGGAATAGCGTGGTGA', 'P0101':'GGCCCAGACTGAGCACGGAATAGCGTGTGA', 'P0110':'GGCCCAGACTGAGCACGGAATAGTATGA', 'P0111':'GGCCCAGACTGAGCACGGAATAGATTGA', 'P1000':'GGCCCAGACTGAGCACGAGGGTGA', 'P1001':'GGCCCAGACTGAGCACGAGGAGTTGA', 'P1010':'GGCCCAGACTGAGCACGTGA', 'P1011':'GGCCCAGACTGAGCACGTGA', 'P1100':'GGCCCAGACTGAGCACGAATCAGGCCCGTGA', 'P1101':'GGCCCAGACTGAGCACGAATCAGGTGCTGA', 'P1110':'GGCCCAGACTGAGCACGAATGCTCTGA', 'P1111':'GGCCCAGACTGAGCACGAATGCTCGGCTGA' } 
#these_leaves = {'P0000':'GGCCCAGACTGAGCACGGAAATGA', 'P0001':'GGCCCAGACTGAGCACGGAAATGA', 'P0010':'GGCCCAGACTGAGCACGGAACGGTGGTGA', 'P0011':'GGCCCAGACTGAGCACGGAACGGGGATGA', 'P0100':'GGCCCAGACTGAGCACGGAATAGCGTGGTGA', 'P0101':'GGCCCAGACTGAGCACGGAATAGCGTGTGA', 'P0110':'GGCCCAGACTGAGCACGGAATAGTATGA', 'P0111':'GGCCCAGACTGAGCACGGAATAGATTGA', 'P1000':'GGCCCAGACTGAGCACGAGGGTGA', 'P1001':'GGCCCAGACTGAGCACGAGGAGTTGA', 'P1010':'GGCCCAGACTGAGCACGTGA', 'P1011':'GGCCCAGACTGAGCACGTGA', 'P1100':'GGCCCAGACTGAGCACGAATCAGGCCCGTGA', 'P1101':'GGCCCAGACTGAGCACGAATCAGGTGCTGA', 'P1110':'GGCCCAGACTGAGCACGAATGCTCTGA', 'P1111':'GGCCCAGACTGAGCACGAATGCTCGGCTGA'}

orig_tree = {'p0': 'GGCCCAGACTGAGCACGTGA', 'p1': 'GGCCCAGACTGAGCACGTGA', 'p000': 'GGCCCAGACTGAGCACGCCTATGA', 'p001': 'GGCCCAGACTGAGCACGCGACTGA', 'p011': 'GGCCCAGACTGAGCACGACTATGA', 'p11': 'GGCCCAGACTGAGCACGCTATGA', 'p010': 'GGCCCAGACTGAGCACGACGTGA', 'p101': 'GGCCCAGACTGAGCACGCGGGACTGA', 'p110': 'GGCCCAGACTGAGCACGCTAAATGA', 'p100': 'GGCCCAGACTGAGCACGCGGTGA', 'p10': 'GGCCCAGACTGAGCACGCGGTGA', 'p': 'GGCCCAGACTGAGCACGTGA', 'p01': 'GGCCCAGACTGAGCACGACTGA', 'p00': 'GGCCCAGACTGAGCACGCTGA', 'p111': 'GGCCCAGACTGAGCACGCTATGA'}

these_leaves = {'p000': 'GGCCCAGACTGAGCACGCCTATGA', 'p001': 'GGCCCAGACTGAGCACGCGACTGA', 'p011': 'GGCCCAGACTGAGCACGACTATGA', 'p010': 'GGCCCAGACTGAGCACGACGTGA', 'p101': 'GGCCCAGACTGAGCACGCGGGACTGA', 'p110': 'GGCCCAGACTGAGCACGCTAAATGA', 'p100': 'GGCCCAGACTGAGCACGCGGTGA', 'p111': 'GGCCCAGACTGAGCACGCTATGA'}


'''   
sorted_orig_tree = []
for f in treeutils.gen_postx(2*(2**const.LEVEL)-1).values():
    sorted_orig_tree.append((f, orig_tree[f]))
    
print sorted_orig_tree
'''

rc = RECONSTRUCT()
rc.fill_leaves(these_leaves.values())

rc.gen_layers()

rc.calc_prbs()
rc.reconstruct()
print rc.PRED_TREE

ev = evaluate.Evaluate()
ev.run_evaluate(orig_tree, rc.PRED_TREE, False)

