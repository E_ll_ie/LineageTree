import const
import sequtils
import Nutils
import math
import utils
import numpy as np
# For each sequene I live all possible parents right now
# This way I get all possible parents for wach layer
# TODO:
# I might need to add some condition here, threshold for Length?
def gen_parents(this_seq):
    parents = set()
    ## Adding this part for missing nodes
    #print this_seq
    if len(this_seq) == 0:
        parents.update([("",0.0)])
        return parents
        
        
    # Insertions
    # Loop over all possible positions
    # Loop over all possible length for inserted region in each position
    for init_loc in const.INS_POS_PRB:
        for leng in const.INS_LEN_PRB:
            nuc_probs = 1
            loc = len(this_seq) + init_loc - leng
            this_parent = this_seq[:loc]+ this_seq[loc+leng:]
            inserted = this_seq[loc:loc+leng]
            for let in inserted:
                nuc_probs= nuc_probs*const.NUCL_PRO_INS_DICT[let]  
            #Raw probabilty
            # FIXME
            #this_prob = const.INS_PROB * const.INS_LEN_PRB[leng] * const.INS_POS_PRB[init_loc] 
            # log domain
            this_prob = math.log(const.INS_PROB) + math.log(const.INS_LEN_PRB[leng]) + math.log(const.INS_POS_PRB[init_loc]) + math.log(nuc_probs)
            if this_prob > float('-inf'):
                parents.update([(this_parent, this_prob)])

    
    # Deletion
    # This is harder, since I have to add N characters.
    # Loop over all possible positions
    # Loop over all possible length for deleted region in each position
    
    # If a sequence have a large deletion, it stops working.
    # So if we see something still working, we know it has not gon through long deletion!
    # That's Why I'm putting a condition here:
    # FIXME: this min length does not look pretty
    #print this_seq
    if not this_seq.endswith("N") and len(this_seq)-this_seq.count("N") > 16 :
        lengs_prob = const.DEL_LEN_PRB.keys()
    else:
        lengs_prob = [i for i in const.DEL_LEN_PRB.keys() if i < 5]
    for init_loc in const.DEL_POS_PRB:
        for leng in lengs_prob:
            for del_dir in const.DEL_DIR_PRB:
                nuc_probs = 1
                loc = len(this_seq) + init_loc +leng
                if del_dir == "R" :
                    this_parent = this_seq[:loc]+ "N"*min(leng,-1*init_loc) + this_seq[loc:]
                else:
                    this_parent = this_seq[:loc-leng]+ "N"*leng + this_seq[loc-leng:]
                for let in range(leng):
                    nuc_probs= nuc_probs*const.NUCL_PRO_INS_DICT["N"]  
                # raw probs
                ##this_prob = const.DEL_PROB * const.DEL_LEN_PRB[leng] * const.DEL_POS_PRB[init_loc] 
                # log domain            
                this_prob = math.log(const.DEL_PROB) + math.log(const.DEL_LEN_PRB[leng]) + math.log(const.DEL_POS_PRB[init_loc]) +\
                    math.log(nuc_probs) +math.log(const.DEL_DIR_PRB[del_dir])
    
                if this_prob > float('-inf'):
                    parents.update([(this_parent, this_prob)])
    
    # No Change
    # Just in case
    #raw probs
    #parents.update([(this_parent, const.NO_PROB)])
    parents.update([(this_seq, math.log(const.NO_PROB))])
    
    return set(sumprobs_common_parents(parents))
    #return parents  


    
# Compare Parent to choose the common ones
# Return a list of common parents
def compare_parents(parentsA, parentsB):
    common_parents = []
    for a_parent in parentsA:
        for b_parent in parentsB:
            if sequtils.compare_seqs(list(a_parent)[0], list(b_parent)[0]):
                this_common_parent = sequtils.choose_less_amb_one(list(a_parent)[0], list(b_parent)[0])
                # raw probs
                ##if list(a_parent)[1]*list(b_parent)[1] > 0 :
                # log domain
                if list(a_parent)[1] + list(b_parent)[1] > float('-inf') :
                    #print a_parent, b_parent, this_common_parent
                    common_parents.append((this_common_parent, list(a_parent)[1]+list(b_parent)[1]))
    return common_parents


def compare_parents_withPRB(parentsA, parentsB):
    common_parents = []

    for a_parent in parentsA:
        for b_parent in parentsB:
            if sequtils.compare_seqs(list(a_parent)[0], list(b_parent)[0]):
                this_common_parent = sequtils.choose_less_amb_one(list(a_parent)[0], list(b_parent)[0])                
                ##if list(a_parent)[1]*list(b_parent)[1] > 0 :
                if list(a_parent)[1]+list(b_parent)[1] > float('-inf') :
                    #print a_parent, b_parent, this_common_parent
                    #multiplication to sum for log
                    common_parents.append((this_common_parent, list(a_parent)[1]+list(b_parent)[1]))
            if len(list(a_parent)[0]) == 0 and len(list(b_parent)[0]) > 0:
                if list(a_parent)[1]+list(b_parent)[1] > float('-inf') :
                    common_parents.append((list(b_parent)[0], list(a_parent)[1]+list(b_parent)[1]))
            if len(list(a_parent)[0]) > 0  and len(list(b_parent)[0]) == 0:
                if list(a_parent)[1]+list(b_parent)[1] > float('-inf') :
                    common_parents.append((list(a_parent)[0], list(a_parent)[1]+list(b_parent)[1]))
    if ('',0) in parentsA or ('', 0) in parentsB:
        common_parents.append(("", 0))              

    #print common_parents
    return common_parents
        
# for the parents that can be generated in different ways
# combine their probabilities 
# Add them up
from collections import defaultdict
def sumprobs_common_parents(common_parents):
    temp_dict = defaultdict(list)
    for field in common_parents:
        seq = field[0]
        prob = field[1]
        temp_dict[seq]+= [prob]
            
    temp_list = []
    for field in temp_dict:
        #raw probs
        #temp_list.append((field, temp_dict[field]))
        #log domain 
        #temp_list.append((field, max(temp_dict[field])))
        temp_list.append((field, utils.logSumExp(temp_dict[field])))
        if np.isnan(utils.logSumExp(temp_dict[field])):
            print temp_dict[field]
        if np.isinf(utils.logSumExp(temp_dict[field])):
            print temp_dict[field]
    #print temp_list
    temp_list.sort(key=lambda x: x[1], reverse= True)
    return temp_list[:const.IMPT_SAMP]


#print [i for i in gen_parents("GGCCCAGACTGAGCACGCCGACTGA")]