import const
import sequtils
def readjust_deletions(pre_layer, this_layer):
    all_addition = []
    all_processed = set()
    for parent in pre_layer:
        parent_to_be_checked = parent
        for child in this_layer:
            if "N" in parent:
                addition = regen_dels_part(parent, child)
                if len(addition) > 0 :
                    all_addition += addition
                    all_processed.update([parent_to_be_checked])
    return list(all_processed), all_addition     
        
                
# this is called when regenrating deleted parts                
def make_new_parent(parent, child, loc, leng):
    new_parent = ""
    for pos in range(len(parent[:loc])):
        if parent[pos] != "N":
            new_parent += parent[pos]
        else:
            new_parent += child[pos]
        new_parent += parent[loc:loc+leng]
    for pos in range(loc+leng, len(parent)):
        if parent[pos] != "N":
            new_parent += parent[pos]
        else:
            new_parent += child[pos-leng] 
    
    return new_parent
        
# replace NNs with what we can get from childrens (adds lots of cases)
def regen_dels_part(parent, child):
    # Case of Deletion
    updateds = []
    if len(parent) > len(child):
        len_diff = len(parent) - len(child)
        if len_diff in const.DEL_LEN_PRB.keys():
            for loc in const.DEL_POS_PRB.keys():
                temp = parent[0:loc] + parent[loc+len_diff:len(parent)+1]
                flagged = sequtils.compare_seqs(temp, child)
                if flagged:
                    temp_gen_parent = make_new_parent(parent, child, loc, len_diff)
                    if temp_gen_parent != parent: 
                        updateds.append(temp_gen_parent)
    # Case of Insertion
    elif len(parent) < len(child):
        len_diff = len(child) - len(parent)
        if len_diff in const.INS_LEN_PRB.keys():
            for loc in const.INS_POS_PRB.keys():
                temp = child[0:loc] + child[loc+len_diff:len(child)+1]
                flagged = sequtils.compare_seqs(temp, parent)
                if flagged:
                    temp_gen_parent = make_new_parent(parent, temp, len(parent), 1)
                    if temp_gen_parent != parent :
                        updateds.append(temp_gen_parent)
                                        
    elif len(parent) == len(child) :
        temp = child
        flagged = sequtils.compare_seqs(temp, parent)
        if flagged:
            temp_gen_parent = make_new_parent(parent, temp, len(parent), 1)
            if temp_gen_parent != parent :
                updateds.append(temp_gen_parent)
    
    return updateds

